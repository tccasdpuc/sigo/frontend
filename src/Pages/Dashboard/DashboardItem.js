import React, { PureComponent } from "react";
import { Grid, Paper, Typography, Divider, CircularProgress, withStyles, withTheme } from "@material-ui/core/";
import { ResponsiveContainer } from 'recharts';

export const DashboardLoading = () => (
    <div style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        zIndex: 100000,
        borderRadius: '5px'
    }}>
        <CircularProgress size={50} />
    </div>
);

export const DashboardLoadingContext = React.createContext({ loading: false });

const styles = theme => ({
    paperChart: {
        padding: theme.spacing(2),
        textAlign: 'center',
        fontSize: '12px',
        color: theme.palette.text.secondary,
        position: 'relative'
    },
    tituloWidgetDashboard: {
        paddingBottom: '10px',
        textAlign: 'left',
        overflow: 'hidden'
    },
    responsiveContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class DashboardItem extends PureComponent {

    render() {
        const { classes, title, gridSize, gridSizeH, moreInfo, extraStyleTypographyTitle, gridSizeLg, gridSizeMd, gridSizeSm, gridSizeXs, children } = this.props;
        var { gridSizeLgVar, gridSizeMdVar, gridSizeSmVar, gridSizeXsVar } = 0;

        if (gridSize === 3) {
            gridSizeLgVar = gridSizeLg || 3;
            gridSizeMdVar = gridSizeMd || 6;
            gridSizeSmVar = gridSizeSm || 12;
            gridSizeXsVar = gridSizeXs || 12;
        } else if (gridSize === 4) {
            gridSizeLgVar = gridSizeLg || 6;
            gridSizeMdVar = gridSizeMd || 12;
            gridSizeSmVar = gridSizeSm || 12;
            gridSizeXsVar = gridSizeXs || 12;
        } else {
            gridSizeLgVar = gridSizeLg || 12;
            gridSizeMdVar = gridSizeMd || 12;
            gridSizeSmVar = gridSizeSm || 12;
            gridSizeXsVar = gridSizeXs || 12;
        }

        return (
            <Grid item xl={gridSize || 12} lg={gridSizeLgVar} md={gridSizeMdVar} sm={gridSizeSmVar} xs={gridSizeXsVar} style={{ overflow: 'hidden' }}>
                <Paper className={classes.paperChart}>
                    <Grid container>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Typography variant="h6" style={extraStyleTypographyTitle} className={classes.tituloWidgetDashboard} color="inherit">{title}</Typography>
                        </Grid>
                        <Grid style={{ textAlign: 'right' }} item xl={1} lg={1} md={1} sm={1} xs={1}>
                            {moreInfo || ''}
                        </Grid>
                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Divider />
                        </Grid>
                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                            <DashboardLoadingContext.Consumer>
                                {({ loading }) => (
                                    <ResponsiveContainer className={classes.responsiveContainer} width="100%" height={gridSizeH * 100} >
                                        {loading ? <DashboardLoading /> : children}
                                    </ResponsiveContainer>
                                )}
                            </DashboardLoadingContext.Consumer>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid >
        );
    };
}

export default withTheme(withStyles(styles)(DashboardItem));