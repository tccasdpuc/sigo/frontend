import React from "react";
import { BarChart, Bar, YAxis, XAxis, Tooltip, Legend } from 'recharts';
import DashboardItem from "./../DashboardItem";

export function BarChartItem(props) {
    const { title, data, xAxisLabel, gridSize, gridSizeH, gridSizeLg, gridSizeMd, gridSizeSm, gridSizeXs, moreInfo, lines, legend, toolTip } = props;
    return (
        <DashboardItem title={title} gridSize={gridSize} gridSizeH={gridSizeH} gridSizeLg={gridSizeLg} gridSizeMd={gridSizeMd} gridSizeSm={gridSizeSm} gridSizeXs={gridSizeXs} moreInfo={moreInfo}>
            <BarChart data={data} margin={{ top: 20, right: 20, bottom: 5, left: 0 }}>
                {xAxisLabel && <XAxis dataKey="name" label={xAxisLabel} />}
                <YAxis />
                {toolTip && <Tooltip />}
                {legend && <Legend />}
                {lines && lines.map((obj, i) => {
                    return <Bar key={i} name={obj.name} type="monotone" dataKey={obj.datakey} fill={obj.stroke} label={{ position: 'top' }} />
                })}
            </BarChart>
        </DashboardItem>
    )
}