import React, { Component } from "react";
import { LineChart, Line, CartesianGrid, YAxis, XAxis, Tooltip, Legend } from 'recharts';
import DashboardItem from "../DashboardItem";
import { objectEquals } from "../../../utils";

export class LineChartItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { title, data, useCartesianGrid, useDot, xAxisLabel, gridSize, gridSizeH, gridSizeLg, gridSizeMd, gridSizeSm, gridSizeXs, moreInfo, lines } = this.props;
        return (
            <DashboardItem title={title} gridSize={gridSize} gridSizeH={gridSizeH} gridSizeLg={gridSizeLg} gridSizeMd={gridSizeMd} gridSizeSm={gridSizeSm} gridSizeXs={gridSizeXs} moreInfo={moreInfo}>
                <LineChart data={data} margin={{ top: 20, right: 20, bottom: 5, left: 0 }}>
                    {lines && lines.map((obj, i) => {
                        return <Line key={i} name={obj.name} type="monotone" dataKey={obj.datakey} stroke={obj.stroke} dot={useDot} />
                    })}
                    {useCartesianGrid && <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />}
                    <XAxis dataKey="name" label={xAxisLabel} />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                </LineChart>
            </DashboardItem>
        )
    }
}