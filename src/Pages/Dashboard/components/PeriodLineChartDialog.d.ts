import React from 'react';
import { PeriodFormValues } from '../../../components/FormControl/PeriodFormDialog';

interface PeriodLineChartDialogProps {
    show: Boolean;
    title: string;
    dtIni: Date;
    dtFin: Date;
    data: [];
    dataKeyX: string;
    dataKeyY: string;
    dataLabel: string;
    domainY: [number, number];
    onPeriodFilterChange: (values: PeriodFormValues) => void;
    onClose: () => void;
}

export default class PeriodLineChartDialog extends React.Component<PeriodLineChartDialogProps> { }