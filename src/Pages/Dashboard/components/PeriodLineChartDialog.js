import React from 'react';
import { Dialog, DialogContent, DialogTitle, Tooltip as TooltipCore, IconButton, FormControl } from "@material-ui/core";
import FilterIcon from '@material-ui/icons/FilterList';
import { LineChart, Line, CartesianGrid, YAxis, XAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import PeriodFormDialog from '../../../components/FormControl/PeriodFormDialog';

export default class PeriodLineChartDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            periodFilterDialogOpen: false
        }
        this.openPeriodFilterDialog = this.openPeriodFilterDialog.bind(this);
    }

    openPeriodFilterDialog() {
        this.setState({ periodFilterDialogOpen: true });
    }

    closePeriodFilterDialog() {
        this.setState({ periodFilterDialogOpen: false });
    }

    render() {
        const { show, title, dtIni, dtFin, data, dataKeyX, dataKeyY, dataLabel, domainY, onPeriodFilterChange, onClose } = this.props;
        const { periodFilterDialogOpen } = this.state;
        return (
            <>
                <Dialog
                    open={show}
                    onClose={onClose}
                    aria-labelledby="responsive-dialog-title"
                    maxWidth='xl'
                    fullWidth
                    PaperProps={{
                        style: {
                            overflowY: 'unset',
                            height: '400px'
                        }
                    }}
                >
                    <DialogTitle>
                        {title}
                        <TooltipCore style={{ float: 'right' }} title="Filtrar">
                            <IconButton
                                size='small'
                                onClick={this.openPeriodFilterDialog}
                                color="inherit">
                                <FilterIcon />
                            </IconButton>
                        </TooltipCore>
                    </DialogTitle>
                    <DialogContent style={{ overflowY: 'unset' }}>
                        {show && <ResponsiveContainer>
                            <LineChart data={data} margin={{ top: 20, right: 20, bottom: 5, left: 0 }} >
                                <Line name={dataLabel} type="monotone" dataKey={dataKeyY} stroke="#8884d8" dot={false} />
                                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                                <XAxis dataKey={dataKeyX} />
                                <YAxis domain={domainY} scale="linear" label={{ value: dataLabel, angle: -90, position: 'insideLeft' }} />
                                <Tooltip />
                            </LineChart>
                        </ResponsiveContainer>}
                    </DialogContent>
                </Dialog>
                <PeriodFormDialog
                    dtIni={dtIni} dtFin={dtFin}
                    show={periodFilterDialogOpen}
                    onSubmit={(values) => {
                        this.closePeriodFilterDialog();
                        if (values && typeof onPeriodFilterChange === 'function') {
                            onPeriodFilterChange(values);
                        }
                    }}
                />
            </>
        )
    }
}