import React, { Component } from "react";
import { PieChart, Pie, Sector, Legend, Tooltip } from 'recharts';
import DashboardItem from '../DashboardItem';

export class CustomActiveShapePieChartItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeIndex: null
        }
        this.onPieLeave = this.onPieLeave.bind(this);
        this.onPieEnter = this.onPieEnter.bind(this);
        this.renderActiveShape = this.renderActiveShape.bind(this);
    }

    onPieEnter(data, index) {
        this.setState({
            activeIndex: index
        });
    }

    onPieLeave(data, index) {
        this.setState({
            activeIndex: null
        });
    }

    renderActiveShape(props) {
        const RADIAN = Math.PI / 180;
        const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
            fill, payload, percent, value, name } = props;

        const { rateName } = this.props;

        const sin = Math.sin(-RADIAN * midAngle);
        const cos = Math.cos(-RADIAN * midAngle);
        const sx = cx + (outerRadius + 10) * cos;
        const sy = cy + (outerRadius + 10) * sin;
        const mx = cx + (outerRadius + 30) * cos;
        const my = cy + (outerRadius + 30) * sin;
        const ex = mx + (cos >= 0 ? 1 : -1) * 22;
        const ey = my;
        const textAnchor = cos >= 0 ? 'start' : 'end';

        return (
            <g>
                <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                <Sector
                    cx={cx}
                    cy={cy}
                    innerRadius={innerRadius}
                    outerRadius={outerRadius}
                    startAngle={startAngle}
                    endAngle={endAngle}
                    fill={fill}
                />
                <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
                <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
                <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`${value} ${name}`}</text>
                <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
                    {`(${rateName ? rateName : ''}${(percent * 100).toFixed(2)}%)`}
                </text>
            </g>
        );
    }

    render() {
        const { data, title, gridSize, gridSizeH, activeIndex, legendOn, gridSizeLg, gridSizeMd, gridSizeSm, gridSizeXs } = this.props;
        return (
            <DashboardItem title={title} gridSize={gridSize} gridSizeH={gridSizeH} gridSizeLg={gridSizeLg} gridSizeMd={gridSizeMd} gridSizeSm={gridSizeSm} gridSizeXs={gridSizeXs}>
                <PieChart>
                    <Pie
                        activeIndex={activeIndex || this.state.activeIndex}
                        activeShape={this.renderActiveShape}
                        dataKey='value'
                        nameKey='key'
                        data={data}
                        outerRadius={80}
                        fill="#8884d8"
                        onMouseEnter={this.onPieEnter}
                        onMouseLeave={this.onPieLeave}
                    />
                    <Tooltip />
                    {legendOn && <Legend />}
                </PieChart>
            </DashboardItem>
        );
    }
}