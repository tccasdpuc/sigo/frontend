import React, { Component } from "react";
import { withSnackbar } from 'notistack';
import { loadingAction } from "../../reducer/loading";
import { connect } from 'react-redux';
import { withTheme, withStyles, Grid } from "@material-ui/core/";

const styles = theme => ({
    paperChart: {
        padding: theme.spacing(2),
        textAlign: 'center',
        fontSize: '12px',
        color: theme.palette.text.secondary,
        position: 'relative'
    },
    paperChartHeader: {
        padding: 5,
        textAlign: 'center',
        fontSize: '12px',
        color: theme.palette.text.secondary,
        position: 'relative',
    },
    root: {
        flex: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        flexDirection: 'column',
        display: 'flex',
        overflowY: 'auto',
        height: '100%'
    },
    navIconHide: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    content: {
        backgroundColor: theme.palette.background.default,
        padding: 8,
        overflowY: 'auto',
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    geral: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: theme.palette.background.default
    },
    tituloWidgetDashboard: {
        paddingBottom: '10px',
        textAlign: 'left',
        overflow: 'hidden'
    },
    responsiveContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class Dashboard extends Component {

    constructor(props) {
        super(props);        
    }

    render() {
        var { classes } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.geral}>
                    <main className={classes.content}>
                        <Grid container spacing={2}>
                            
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggedIn, user } = state.authentication;
    return {
        loggedIn,
        operator: user
    };
}

const Connected = withTheme(withStyles(styles)(withSnackbar(connect(mapStateToProps, { loadingAction })(Dashboard))));

export {
    Connected as Dashboard
}