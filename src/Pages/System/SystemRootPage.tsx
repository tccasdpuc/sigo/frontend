import React, { Component } from "react";
import { Typography } from "@material-ui/core";
import { Panel } from "../../components";


export default class SystemRootPage extends Component {
    render() {
        return (
            <Panel
                panelHeaderTitle="SIGO"
            >
                <Typography>
                    Bem vindo ao Sistema Integrado de Gestão e Operação
            </Typography>
            </Panel>
        );
    }
}