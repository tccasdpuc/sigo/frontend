import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, RouteChildrenProps, Redirect } from 'react-router-dom';
import { Header } from '../../components';
import AuthService from "../../service/AuthService";
import { logoutAction, AuthenticationState } from "../../reducer/authentication";
import { loadingAction } from "../../reducer/loading";
import DrawerNavigator, { NavigatorMenuItem } from '../../components/DrawerNavigator';
import { RootState } from '../../reducer';
import { faFileAlt, faBriefcase, faIndustry, faDatabase } from '@fortawesome/free-solid-svg-icons'
import NormasPage from './Normas';
import RelatoriosPage from './Relatorios';
import SystemRootPage from './SystemRootPage';
import ConsultoriasPage from './Consultorias';

interface SystemProps extends RouteChildrenProps, AuthenticationState {
    loadingAction: typeof loadingAction;
    logoutAction: typeof logoutAction;
}

interface SystemState {
    menuOpen: boolean;
}

class System extends Component<SystemProps, SystemState> {

    private authTimer: NodeJS.Timer | null;

    constructor(props: SystemProps) {
        super(props);
        this.state = {
            menuOpen: false
        }
        this.onItemMenuClick = this.onItemMenuClick.bind(this);
        this.logOut = this.logOut.bind(this);
        this.validateToken = this.validateToken.bind(this);
        this.authTimer = null;
    }

    async logOut() {
        var { loadingAction, logoutAction } = this.props;
        loadingAction(true);
        await AuthService.instance.logout();
        loadingAction(false);
        logoutAction();
    }

    componentDidMount() {
        this.validateToken();
        this.authTimer = setInterval(this.validateToken, 60 * 1000);
    }

    async validateToken() {
        var authOk = await AuthService.instance.validToken();
        if (authOk === false) {
            await this.logOut();
        }
    }

    componentWillUnmount() {
        if (this.authTimer) {
            clearInterval(this.authTimer);
        }
    }

    onMenuClick() {
        const { menuOpen } = this.state;
        this.setState({
            menuOpen: !menuOpen
        });
    }

    onItemMenuClick(item: NavigatorMenuItem, closeMenu: boolean = false) {
        const { history } = this.props;
        if (closeMenu === true) {
            this.setState({ menuOpen: false }, () => {
                history.push(item.url);
            });
        } else {
            history.push(item.url);
        }
    }

    getMenuList() {
        const menuList: NavigatorMenuItem[] = [];
        menuList.push({ label: 'Normas', url: '/system/normas', IconProp: faFileAlt },
            { label: 'Consultorias/Acessorias', url: '/system/consultorias', IconProp: faBriefcase },
            { label: 'Gestão', url: '/system/gestao', IconProp: faIndustry },
            { label: 'Relatórios', url: '/system/relatorios', IconProp: faDatabase });
        return menuList;
    }

    render() {
        const { match } = this.props;
        if (!match) return null;
        const { menuOpen } = this.state;
        const menuList = this.getMenuList();
        return (
            <>
                <Header onMenuClick={() => this.onMenuClick()} />
                <div style={{ flex: 1, display: 'flex', overflow: 'hidden' }}>
                    <DrawerNavigator
                        menuList={menuList}
                        open={menuOpen}
                        onItemMenuClick={this.onItemMenuClick}
                    />
                    <Switch>
                        <Route exact path={match.url} component={SystemRootPage} />
                        <Route path={match.url + '/normas'} component={NormasPage} />
                        <Route path={match.url + '/consultorias'} component={ConsultoriasPage} />
                        <Route path={match.url + '/relatorios'} component={RelatoriosPage} />
                        <Route render={() => <Redirect to={match?.url || ""} />} />
                    </Switch>
                </div>
            </>
        )
    }

}

function mapStateToProps(state: RootState) {
    return {
        ...state.authentication
    };
}

const mapDispatchToProps = {
    loadingAction,
    logoutAction
};

export default connect(mapStateToProps, mapDispatchToProps)(System);