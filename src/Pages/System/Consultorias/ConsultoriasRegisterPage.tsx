import TextField from "@material-ui/core/TextField";
import { number } from "prop-types";
import React, { Component } from "react";
import { RouteChildrenProps } from "react-router-dom";
import { Panel } from "../../../components";
import DefaultButton from "../../../components/FormControl/DefaultButton";
import SelectSearch from "../../../components/SelectSearch/SelectSearch";
import Consultoria from "../../../model/Consultoria";
import Contrato from "../../../model/Contrato";
import { SigoApi } from "../../../service/SigoApi";
import { SigoApi as SigoApiWithLoading } from "../../../service/SigoApiWithLoading";

interface ConsultoriasRegisterPageState {
    descricao: string;
    contratoId: number;
}

export default class ConsultoriasRegisterPage extends Component<RouteChildrenProps, ConsultoriasRegisterPageState>{

    private service: SigoApi<Consultoria>;

    constructor(props: RouteChildrenProps) {
        super(props);
        this.state = {
            descricao: '',
            contratoId: 1
        }

        this.service = new SigoApiWithLoading<Consultoria>('consultorias-api/consultoria');
    }

    getContratos(): Contrato[] {
        return [
            { id: 1, descricao: 'I9 Consultoria' },
            { id: 2, descricao: 'ECR Consultoria e Treinamento' },
            { id: 3, descricao: 'Contabily - Contabilidade' },
        ];
    }

    async onFormBuscaSubmit() {
        const { history } = this.props;
        const { contratoId, descricao } = this.state;
        await this.service.insert({
            contratoId,
            descricao,
            data: new Date().toJSON()
        });
        history.push("/");
    }

    render() {
        const { contratoId, descricao } = this.state;
        return (
            <Panel
                panelHeaderTitle='Registro de Consultoria'
            >
                <form onSubmit={(e) => {
                    this.onFormBuscaSubmit();
                    e.preventDefault();
                    e.stopPropagation();
                }}>
                    <SelectSearch<number>
                        name='contratoId'
                        label='Categoria'
                        value={contratoId}
                        onChangeHandle={(_, value) => { if (value) this.setState({ contratoId: value }); }}
                        options={this.getContratos().map(c => ({ label: c.descricao, value: c.id }))}
                        margin='dense'
                        isClearable={false}
                        fullWidth
                    />
                    <TextField
                        name="descricao"
                        label="Descrição"
                        margin="dense"
                        variant="outlined"
                        required
                        fullWidth
                        value={descricao}
                        onChange={(ev) => this.setState({ descricao: ev.target.value })}
                        autoComplete='off'
                    />
                    <DefaultButton
                        type='submit'
                        size='small'
                        variant="contained"
                    >Registrar</DefaultButton>
                </form>
            </Panel>
        );
    }

}