import React, { Component } from "react";
import { Redirect, Route, RouteChildrenProps, Switch } from "react-router-dom";
import ConsultoriasRegisterPage from "./ConsultoriasRegisterPage";

export default class ConsultoriasPage extends Component<RouteChildrenProps>{
    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route exact path={match?.url + "/"} component={ConsultoriasRegisterPage} />
                <Route render={() => <Redirect to={match?.url || ""} />} />
            </Switch>
        );
    }
}