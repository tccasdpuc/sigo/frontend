import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import { Typography, Grid } from '@material-ui/core';
import { Panel } from '../../../components';

function NotFound() {
    return (
        <Grid container spacing={0} >
            <Grid item xs={12} className="content">
                <Panel>
                    <Typography variant="h4" align='center'>Página não encontrada</Typography>
                </Panel>
            </Grid>
        </Grid>
    );
}

NotFound.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotFound);