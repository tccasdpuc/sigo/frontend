import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid } from '@material-ui/core';
import { Panel } from '../../../components';

function NotAuthorized() {
    return (
        <Grid container spacing={0} >
            <Grid item xs={12} className="content">
                <Panel>
                    <Typography variant="h4" align='center'>Você não possui permissão para acessar este recurso!</Typography>
                    <Typography variant="h6" align='center'>Contate um administrador do sistema.</Typography>
                </Panel>
            </Grid>
        </Grid>
    );
}

NotAuthorized.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default NotAuthorized;