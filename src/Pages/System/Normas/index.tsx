import React, { Component } from 'react';
import NormasList from './NormasList';
import { Redirect, Route, RouteChildrenProps, Switch } from 'react-router-dom';
import NormasExternas from './NormasExternas';

export interface NormasPageProps extends RouteChildrenProps {

}

export default class NormasPage extends Component<NormasPageProps>{

    constructor(props: NormasPageProps) {
        super(props);
    }

    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route exact path={match?.url + "/"} component={NormasList} />
                <Route exact path={match?.url + "/externas"} component={NormasExternas} />
                <Route render={() => <Redirect to={match?.url || ""} />} />
            </Switch>
        );
    }

}