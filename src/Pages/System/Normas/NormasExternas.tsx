import { TextField, Typography } from "@material-ui/core";
import React, { Component } from "react"
import { RouteChildrenProps } from "react-router-dom";
import DefaultButton from "../../../components/FormControl/DefaultButton";
import Panel from "../../../components/Panel";
import SelectSearch from "../../../components/SelectSearch";
import CategoriaNorma from "../../../model/CategoriaNorma";
import NormaExterna from "../../../model/NormaExterna";
import NormasService from "../../../service/NormasService";

export interface NormasExternasProps extends RouteChildrenProps {

}

export interface NormasExternasState {
    busca: string;
    codigoCategoria: string;
    normas: NormaExterna[] | null;
}

export interface NormasExternasFormData {
    busca: string;
    codigoCategoria: string
}

interface NormaExternaViewProps {
    norma: NormaExterna,
    onClick: (norma: NormaExterna) => any
}

function NormaExternaView(props: NormaExternaViewProps): React.FunctionComponentElement<NormaExternaViewProps> {
    const { norma: { autor, codigo, titulo, urlThumbnail }, onClick } = props;
    return (
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            borderBottom: '1px solid gray',
            marginTop: 2,
            marginBottom: 2,
            paddingTop: 2,
            paddingBottom: 2,
        }}
            onClick={() => onClick(props.norma)}
        >
            <img style={{ border: '1px solid gray' }} src={urlThumbnail} width={100} height={140} ></img>
            <div style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                paddingLeft: 8
            }}>
                <Typography><b>Código: </b>{codigo}</Typography>
                <Typography><b>Título: </b>{titulo}</Typography>
                <Typography><b>Autor: </b>{autor}</Typography>
            </div>
        </div>
    );
}

export default class NormasExternas extends Component<NormasExternasProps, NormasExternasState> {

    private service: NormasService;

    constructor(props: NormasExternasProps) {
        super(props);
        this.state = {
            busca: '',
            codigoCategoria: 'nbr',
            normas: null
        }

        this.service = new NormasService();
    }

    getCategorias(): CategoriaNorma[] {
        return [
            {
                "nome": "Normas ABNT NBR/NM",
                "codigo": "nbr",
                "totalRegistrosEncontrados": 2
            },
            {
                "nome": "Normas Internacionais/Estrangeiras",
                "codigo": "nie",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Documentos Internos",
                "codigo": "doc",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "e-Books ASQ",
                "codigo": "asq",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Publicações do Diário Oficial da União",
                "codigo": "dou",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Distrito Federal",
                "codigo": "dodf",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Estado de Minas Gerais",
                "codigo": "doemg",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Estado do Rio de Janeiro",
                "codigo": "doerj",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Estado do Rio Grande do Sul",
                "codigo": "doers",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município de Campinas",
                "codigo": "domcamp",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município de Guarulhos",
                "codigo": "domgru",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município de Osasco",
                "codigo": "domosasco",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município de Porto Alegre",
                "codigo": "dompoa",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município do Rio de Janeiro",
                "codigo": "domrj",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Município de São Paulo",
                "codigo": "domsp",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Diário Oficial do Estado de São Paulo",
                "codigo": "dosp",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Projetos de Normas ABNT NBR",
                "codigo": "pn",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Regulamentos Técnicos do INMETRO",
                "codigo": "rt",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Normas Regulamentadoras do MTE",
                "codigo": "nr",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Legislações da ONS",
                "codigo": "ons",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Legislações da ANVISA",
                "codigo": "anvisa",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Legislações do MAPA",
                "codigo": "mapa",
                "totalRegistrosEncontrados": 0
            },
            {
                "nome": "Legislações do CONAMA",
                "codigo": "conama",
                "totalRegistrosEncontrados": 0
            }
        ];
    }

    async onFormBuscaSubmit() {
        const { busca, codigoCategoria } = this.state;
        const normas = await this.getNormasExternas(busca, codigoCategoria);
        if (Array.isArray(normas)) {
            this.setState({ normas });
        }
    }

    async getNormasExternas(busca: string, codigoCategoria: string): Promise<NormaExterna[]> {
        return this.service.getNormasExternas(busca, codigoCategoria);
    }

    onNormaExternaClick(norma: NormaExterna) {

    }

    render() {
        const { busca, codigoCategoria, normas } = this.state;
        return (
            <Panel
                panelHeaderTitle="Base de dados externa de Normas"
            >
                <div>
                    <form onSubmit={(e) => {
                        this.onFormBuscaSubmit();
                        e.preventDefault();
                        e.stopPropagation();
                    }}>
                        <SelectSearch<string>
                            name='codigoCategoria'
                            label='Categoria'
                            value={codigoCategoria || null}
                            onChangeHandle={(_, value) => this.setState({ codigoCategoria: value || '' })}
                            options={this.getCategorias().map(c => ({ label: c.nome, value: c.codigo }))}
                            margin='dense'
                            isClearable={false}
                            fullWidth
                        />
                        <TextField
                            name="busca"
                            label="Busca"
                            margin="dense"
                            variant="outlined"
                            required
                            fullWidth
                            value={busca}
                            onChange={(ev) => this.setState({ busca: ev.target.value })}
                            autoComplete='off'
                        />
                        <DefaultButton
                            type='submit'
                            size='small'
                            variant="contained"
                        >Busca</DefaultButton>
                    </form>
                </div>
                <div style={{
                    marginTop: 4,
                    overflowY: 'auto'
                }}>
                    {normas && normas.map((n, i) => <NormaExternaView key={n.codigo + '_' + i} norma={n} onClick={(n) => this.onNormaExternaClick(n)} />)}
                </div>
            </Panel>
        );
    }
}