import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import CloudCircleIcon from "@material-ui/icons/CloudCircle";
import React, { Component } from "react";
import Panel from "../../../components/Panel";
import nbsp from '../../../components/nbsp';
import TableListPsim from "../../../components/TableListPsim/TableListPsim";
import Norma from "../../../model/Norma";
import { RouteChildrenProps } from "react-router-dom";
import NormasService from "../../../service/NormasService";
import { WhereCondition } from "../../../service/SigoApi";


export interface NormasListProps extends RouteChildrenProps {

}

export interface NormasListState {
    busca: string,
    normas: Norma[];
}

class NormasList extends Component<NormasListProps, NormasListState> {

    private service: NormasService;

    constructor(props: NormasListProps) {
        super(props);
        this.onSearch = this.onSearch.bind(this);
        this.state = {
            busca: '',
            normas: []
        }

        this.service = new NormasService();
    }

    async componentDidMount() {
        const normas = await this.getNormas('');
        if (Array.isArray(normas)) {
            this.setState({ normas });
        }
    }

    async onSearch(busca: string) {
        const normas = await this.getNormas(busca);
        if (Array.isArray(normas)) {
            this.setState({ normas });
        }
    }

    async getNormas(busca: string): Promise<Norma[]> {
        const where: WhereCondition<Norma> = [];
        if (busca.trim() !== "") {
            where.push(['nome', 'like', busca]);
        }

        return this.service.getList({ where });

        /*return [
            { id: 1, nome: 'Norma 1', categoria: "NBR", versao: "2018" },
            { id: 2, nome: 'Norma 2', categoria: "NBR", versao: "2019" }
        ];*/
    }

    onExternalSearchClick() {
        const { history, match } = this.props;
        history.push(match?.url + '/externas');
    }

    render() {
        const { normas } = this.state;
        return (
            <Panel
                panelHeaderTitle={nbsp}
                panelHeaderButtons={
                    <Tooltip title="Busca em base de dados externa">
                        <IconButton
                            onClick={() => this.onExternalSearchClick()}
                        >
                            <CloudCircleIcon />
                        </IconButton>
                    </Tooltip>
                }
            >
                <TableListPsim<Norma>
                    title='Normas'
                    onSearch={this.onSearch}
                    noPadding={true}
                    columns={[
                        { value: "#", field: 'id', visible: true, width: 30, alignContent: 'center' },
                        { value: "Nome", field: 'nome', visible: true, alignContent: 'left' },
                        { value: "Categoria", field: 'categoria', width: 100, visible: true, alignContent: 'center' },
                        { value: "Versão", field: 'versao', width: 100, visible: true, alignContent: 'center' },
                    ]}
                    rows={normas}
                />
            </Panel>
        );
    }
}

export default NormasList;