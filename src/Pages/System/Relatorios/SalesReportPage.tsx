import React, { Component } from "react";
import { RouteChildrenProps } from "react-router-dom";
import { Panel } from "../../../components";
import DefaultButton from "../../../components/FormControl/DefaultButton";
import SelectSearch from "../../../components/SelectSearch";
import OptionItem from "../../../components/SelectSearch/OptionItem";
import TableListPsim from "../../../components/TableListPsim/TableListPsim";
import { Sales } from "../../../model/Sales";
import SalesReportService from "../../../service/SalesReportService";

interface SalesReportPageProps extends RouteChildrenProps {

}

interface SaleData {
    id: number,
    salesCount: number;
}

interface SalesReportPageState {
    year: number,
    month: number,
    sales: SaleData[] | null
}


export default class SalesReportPage extends Component<SalesReportPageProps, SalesReportPageState> {

    private service: SalesReportService;

    constructor(props: SalesReportPageProps) {
        super(props);
        this.state = {
            year: 2020,
            month: 10,
            sales: null
        }

        this.service = new SalesReportService();
    }

    getYearOptions(): OptionItem<number>[] {
        return [
            { label: '2020', value: 2020 },
            { label: '2019', value: 2019 },
            { label: '2018', value: 2018 },
            { label: '2017', value: 2017 },
            { label: '2016', value: 2016 },
            { label: '2015', value: 2015 },
        ];
    }

    getMonthOptions(): OptionItem<number>[] {
        return [
            { label: 'Janeiro', value: 1 },
            { label: 'Fevereiro', value: 2 },
            { label: 'Março', value: 3 },
            { label: 'Abril', value: 4 },
            { label: 'Maio', value: 5 },
            { label: 'Junho', value: 6 },
            { label: 'Julho', value: 7 },
            { label: 'Agosto', value: 8 },
            { label: 'Setembro', value: 9 },
            { label: 'Outubro', value: 10 },
            { label: 'Novembro', value: 11 },
            { label: 'Dezembro', value: 12 },
        ];
    }

    async getSales(year: number, month: number): Promise<Sales[]> {
        try {
            const result = await this.service.getReport(year, month);
            if (Array.isArray(result)) {
                return result;
            }
        } catch (e) {
            console.error(e);
        }
        return [];
    }

    async onFormBuscaSubmit() {
        const { year, month } = this.state;
        const sales = await this.getSales(year, month);
        const salesData: SaleData[] = sales.map(s => ({ id: s.productId, salesCount: s.salesCount }));
        this.setState({ sales: salesData });
    }

    render() {
        const { year, month, sales } = this.state;
        return (
            <Panel
                panelHeaderTitle='Relatório de Vendas'
            >
                <div>
                    <form onSubmit={(e) => {
                        this.onFormBuscaSubmit();
                        e.preventDefault();
                        e.stopPropagation();
                    }}>
                        <SelectSearch<number>
                            name='year'
                            label='Ano'
                            value={year}
                            onChangeHandle={(_, value) => {
                                if (value) {
                                    this.setState({ year: value })
                                }
                            }}
                            options={this.getYearOptions()}
                            margin='dense'
                            isClearable={false}
                            fullWidth
                        />
                        <SelectSearch<number>
                            name='month'
                            label='Mês'
                            value={month}
                            onChangeHandle={(_, value) => {
                                if (value) {
                                    this.setState({ month: value })
                                }
                            }}
                            options={this.getMonthOptions()}
                            margin='dense'
                            isClearable={false}
                            fullWidth
                        />
                        <DefaultButton
                            type='submit'
                            size='small'
                            variant="contained"
                        >Busca</DefaultButton>
                    </form>
                </div>
                <br />
                {sales &&
                    <TableListPsim<SaleData>
                        columns={[
                            { field: 'id', width: 150, value: 'Codigo do Produto', alignContent: 'center' },
                            { field: 'salesCount', value: 'Vendas no mês', alignContent: 'center' }
                        ]}
                        rows={sales}
                        noPadding={true}
                    />
                }
            </Panel>
        );
    }

}