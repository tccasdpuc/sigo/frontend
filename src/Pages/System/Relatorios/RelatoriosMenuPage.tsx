import React, { Component } from "react";
import NestedMenu from '../../../components/NestedMenu';
import { RouteChildrenProps } from "react-router-dom";
import { Panel } from "../../../components";

export default class RelatoriosMenuPage extends Component<RouteChildrenProps>{


    onMenuClick(id: any) {
        const { history, match } = this.props;
        history.push(match?.url + "/" + id);
    }

    render() {
        return (
            <Panel
                panelHeaderTitle="Relatórios"
            >
                <NestedMenu
                    list={[
                        { id: 'salesReport', label: 'Vendas', subs: [] }
                    ]}
                    onClickHandle={(id) => this.onMenuClick(id)}
                />
            </Panel>
        );
    }

}