import React, { Component } from "react";
import Panel from '../../../components/Panel';

import { Redirect, Route, RouteChildrenProps, Switch } from "react-router-dom";
import RelatoriosMenuPage from "./RelatoriosMenuPage";
import SalesReportPage from "./SalesReportPage";

export interface RelatoriosPageProps extends RouteChildrenProps {

}

export default class RelatoriosPage extends Component<RelatoriosPageProps> {

    constructor(props: RelatoriosPageProps) {
        super(props);
    }

    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route exact path={match?.url + "/"} component={RelatoriosMenuPage} />
                <Route exact path={match?.url + "/salesReport"} component={SalesReportPage} />
                <Route render={() => <Redirect to={match?.url || ""} />} />
            </Switch>
        );
    }
}