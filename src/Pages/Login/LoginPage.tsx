import React, { Component } from "react";
import { connect } from 'react-redux';
import LoginForm from "./LoginForm";
import AuthService from "../../service/AuthService";
import { successLoginAction, logoutAction, AuthenticationState } from "../../reducer/authentication";
import { loadingAction } from "../../reducer/loading";
import { RouteChildrenProps } from "react-router-dom";
import { RootState } from "../../reducer";

var qp = require('query-parse');

interface LoginPageProps extends AuthenticationState, RouteChildrenProps {
    successLoginAction: typeof successLoginAction;
    logoutAction: typeof logoutAction;
    loadingAction: typeof loadingAction;
}

interface LoginPageState {
    errorMessage: string | null
}

export interface LoginRequest {
    username: string, password: string
}

class LoginPage extends Component<LoginPageProps, LoginPageState> {

    constructor(props: LoginPageProps) {
        super(props);
        this.onLogin = this.onLogin.bind(this);
        this.state = {
            errorMessage: null
        }
    }

    async verifyAuth() {
        var { loggedIn, history, successLoginAction } = this.props;
        if (loggedIn) {
            history.push('/');
        } else {
            var authOk = await AuthService.instance.validToken();
            if (authOk) {
                successLoginAction();
            }
        }
    }

    componentDidMount() {
        this.verifyAuth();
    }

    componentDidUpdate() {
        this.verifyAuth();
    }

    async onLogin({ username, password }: LoginRequest) {
        var { successLoginAction, loadingAction, logoutAction } = this.props;
        try {
            loadingAction(true);
            const loginOk = await AuthService.instance.login(username, password);
            if (loginOk) {
                successLoginAction();
            }
        } catch (e) {
            this.setState({ errorMessage: e.message });
            logoutAction();
        } finally {
            loadingAction(false);
        }
    }

    render() {
        const { errorMessage } = this.state;
        return (
            <div style={{ display: 'flex', alignContent: 'center', justifyContent: 'center', alignItems: 'center', flexGrow: 1 }}>
                <LoginForm
                    onSubmit={this.onLogin}
                    errorMessage={errorMessage}
                />
            </div>
        );
    }
}

function mapStateToProps(state: RootState) {
    const { authentication } = state;
    return authentication;
}

const mapDispatchToProps = {
    loadingAction,
    successLoginAction,
    logoutAction
};

const connected = connect(mapStateToProps, mapDispatchToProps)(LoginPage);
export default connected;