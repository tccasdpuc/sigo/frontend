import React, { Component } from "react";
import PropTypes from 'prop-types';
import { FormControl, Button, TextField, Paper, FormHelperText, withTheme, Theme } from '@material-ui/core/';
import logo from '../../images/logo.png';
import { LoginRequest } from "./LoginPage";
import { Link } from "react-router-dom";

interface LoginFormProps {
    onSubmit: (data: LoginRequest) => any;
    theme: Theme;
    errorMessage: string | null;
}

interface LoginFormState {
    username: string;
    password: string;
    passwordError: boolean;
}

class LoginForm extends Component<LoginFormProps, LoginFormState> {

    constructor(props: LoginFormProps) {
        super(props);
        this.state = {
            username: '',
            password: '',
            passwordError: false
        }

        this.onSubmitHandler = this.onSubmitHandler.bind(this);
        this.onUsernameChangeHandler = this.onUsernameChangeHandler.bind(this);
        this.onPasswordChangeHandler = this.onPasswordChangeHandler.bind(this);
    }

    onUsernameChangeHandler(ev: React.ChangeEvent<HTMLInputElement>) {
        var value = ev.target.value;
        this.setState({
            username: value
        });
    }

    onPasswordChangeHandler(ev: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            password: ev.target.value,
            passwordError: ev.target.value === ''
        });
    }

    onSubmitHandler(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (typeof this.props.onSubmit === 'function') {
            const { username, password } = this.state;
            this.props.onSubmit({ username, password });
        }
    }

    render() {
        const { errorMessage, theme } = this.props;
        const { username, password, passwordError } = this.state;
        const margin = theme.spacing(3);
        return (
            <Paper style={{ width: 400, padding: margin, display: 'flex', flexDirection: 'column' }}>
                <div style={{ display: 'flex', alignContent: 'center', justifyContent: 'center', paddingBottom: margin }}>
                    <img src={logo} alt="SIGO" style={{ height: 60 }} />
                </div>
                <form onSubmit={this.onSubmitHandler} style={{ display: 'flex', flexDirection: 'column' }}>
                    <TextField
                        label='Login'
                        onChange={this.onUsernameChangeHandler}
                        value={username}
                        style={{ paddingBottom: margin }}
                        autoFocus={true}
                        autoComplete='off'
                        required
                    />
                    <TextField
                        label='Senha'
                        onChange={this.onPasswordChangeHandler}
                        value={password}
                        type='password'
                        style={{ paddingBottom: margin }}
                        error={passwordError}
                        autoComplete='off'
                        required
                    />
                    <FormControl >
                        <Button type="submit" variant='contained' color='primary'>Login</Button>
                    </FormControl>
                </form>
                {errorMessage && <FormHelperText error={true}>{errorMessage}</FormHelperText>}                
            </Paper >
        );
    }
}

export default withTheme(LoginForm);