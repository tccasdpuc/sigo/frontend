import React, { Component } from 'react';
import './App.css';
import { HashRouter, Switch, Route, Redirect, RouteChildrenProps } from 'react-router-dom';
import System from './Pages/System';
import OverlayLoading from './components/OverlayLoading/OverlayLoading';
//import MomentUtils from '@date-io/moment';
import moment from 'moment';
import LoginPage from './Pages/Login/LoginPage';
import { connect } from 'react-redux';
import { AuthenticationState, mapAuthenticationStateToProps } from "./reducer/authentication";
import "@kenshooui/react-multi-select/dist/style.css"
import AlertDialog from './components/AlertDialog';
import { ThemeBlue } from './themes/ThemeBlue';
import { MuiThemeProvider } from '@material-ui/core';

import Settings from './service/Settings';

moment.defineLocale('pt-br', {
    months: 'Janeiro_Fevereiro_Março_Abril_Maio_Junho_Julho_Agosto_Setembro_Outubro_Novembro_Dezembro'.split('_'),
    monthsShort: 'Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez'.split('_'),
    weekdays: 'Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado'.split('_'),
    weekdaysShort: 'Dom_Seg_Ter_Qua_Qui_Sex_Sáb'.split('_'),
    weekdaysMin: 'Dom_2ª_3ª_4ª_5ª_6ª_Sáb'.split('_'),
    longDateFormat: {
        LT: 'HH:mm',
        L: 'DD/MM/YYYY',
        LL: 'D [de] MMMM [de] YYYY',
        LLL: 'D [de] MMMM [de] YYYY [às] LT',
        LLLL: 'dddd, D [de] MMMM [de] YYYY [às] LT',
        LTS: 'HH:mm:ss'
    },
    relativeTime: {
        future: 'em %s',
        past: '%s atrás',
        s: 'segundos',
        m: 'um minuto',
        mm: '%d minutos',
        h: 'uma hora',
        hh: '%d horas',
        d: 'um dia',
        dd: '%d dias',
        M: 'um mês',
        MM: '%d meses',
        y: 'um ano',
        yy: '%d anos'
    },
    ordinal: (number: number) => `${number}º`
});

let lang = window.navigator.languages[0].toLowerCase();
if (moment.locales().indexOf(lang) >= 0) {
    moment.locale(lang);
}

interface InitialAppProps extends RouteChildrenProps, AuthenticationState {

}

const Loading = () => <h5>Loading...</h5>;

class InitialApp extends Component<InitialAppProps> {

    componentDidMount() {
        let { loggedIn, history } = this.props;
        if (loggedIn) {
            history.push('/system');
        } else {
            history.push('/login');
        }
    }

    render() {
        return <Loading />;
    }
}

const ConnectedInitialApp = connect(mapAuthenticationStateToProps)(InitialApp);

interface PrivateRouteProps extends AuthenticationState {
    path: string;
    component: React.ComponentType<RouteChildrenProps>;
}

const PrivateRoute = connect(mapAuthenticationStateToProps)((props: PrivateRouteProps) => {
    const { component: Component, loggedIn, ...rest } = props;
    return (
        <Route
            {...rest}
            render={props =>
                loggedIn ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: props.location }
                            }}
                        />
                    )
            }
        />
    );
})


interface AppProps {

}

interface AppState {
    loaded: boolean;
}

class App extends Component<AppProps, AppState> {

    constructor(props: AppProps) {
        super(props);
        this.state = {
            loaded: false
        }
    }

    async componentDidMount() {
        await this.getSettings();
        this.setState({ loaded: true });
    }

    async getSettings() {
        try {
            const response = await fetch('Settings.json');
            if (response.ok) {
                const settings = await response.json();
                Settings.set(settings);
            }
        } catch (e) {
            console.error('getSettings', e);
        }
    }

    render() {
        const { loaded } = this.state;
        if (loaded === false) {
            return <Loading />;
        }
        return (
            <MuiThemeProvider theme={ThemeBlue}>
                <div className="App">
                    <OverlayLoading />
                    <AlertDialog />
                    <HashRouter >
                        <Switch>
                            <Route exact path="/" component={ConnectedInitialApp} />
                            <Route exact path="/login" component={LoginPage} />
                            <PrivateRoute path="/system" component={System} />
                            <Route render={() => <Redirect to="/" />} />
                        </Switch>
                    </HashRouter>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;