export interface Entity { }

export interface GuidIdentityEntity extends Entity {
    guid: string;
}

export interface SoftDeleteEntity {
    /**
     * Data de exclusão
     */
    deletedAt: Date;

    /**
     * Excluido: SIM ou NÃO
     */
    deleted: boolean;
}

export interface TimestampsEntity {

    /**
     * Data de criação
     */
    createdAt: Date;

    /**
     * Data da última atualização
     */
    updatedAt: Date;

}