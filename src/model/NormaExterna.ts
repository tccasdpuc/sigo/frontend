export default interface NormaExterna {
    codigo: string;
    titulo: string;
    autor: string;
    urlThumbnail: string;
}