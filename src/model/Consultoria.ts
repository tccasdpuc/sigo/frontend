import { Entity } from ".";

export default interface Consultoria extends Entity {
    id: number;
    descricao: string;
    contratoId: number;
    data: string;
}