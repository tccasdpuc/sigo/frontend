export default interface CategoriaNorma {
    nome: string;
    codigo: string;
    totalRegistrosEncontrados: number;
}