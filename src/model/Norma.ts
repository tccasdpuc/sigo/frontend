import { Entity } from ".";

export default interface Norma extends Entity {
    id: number;
    nome: string;
    categoria: string;
    versao: string;
}