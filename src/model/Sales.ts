export interface Sales {
    productId: number;
    salesCount: number;
}