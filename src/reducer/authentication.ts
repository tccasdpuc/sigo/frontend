import { Action } from 'redux';
import { RootState } from '.';
import { AuthResponse } from '../service/AuthService';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';
export const PERMISSIONS_SUCCESS = 'PERMISSIONS_SUCCESS';

var token = localStorage.getItem("authTokens");
const authToken = token ? JSON.parse(token) as AuthResponse : null;

const initialState: AuthenticationState = {
    loggedIn: !!(authToken?.accessToken)
};

export function successLoginAction(): AuthenticationAction {
    return {
        type: LOGIN_SUCCESS
    };
}

export function logoutAction(): AuthenticationAction {
    return {
        type: LOGOUT
    }
}

export interface AuthenticationAction extends Action {
    payload?: AuthenticationState
}

export interface LoginAction extends AuthenticationAction {
    type: typeof LOGIN_SUCCESS    
}

export interface LogoutAction extends AuthenticationAction {
    type: typeof LOGOUT
}

export interface AuthenticationState {
    loggedIn?: boolean;
}

export function mapAuthenticationStateToProps(state: RootState) {
    return state.authentication;
}

export function authenticationReducer(state = initialState, action: AuthenticationAction): AuthenticationState {
    const { type, payload } = action;
    switch (type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                loggedIn: true
            };
        case LOGOUT:
            return {
                ...state,
                loggedIn: false,
            };
        default:
            return state;
    }
}

