import { combineReducers, } from 'redux';
import loading from './loading';
import { authenticationReducer } from './authentication';
import { alertDialog } from './alertDialog';

const rootReducer = combineReducers({
    authentication: authenticationReducer,    
    loading,
    alertDialog,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;