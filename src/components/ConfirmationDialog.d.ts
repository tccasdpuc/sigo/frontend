import React from 'react';
import { ReactConfirmProps } from 'react-confirm';
import { ButtonProps } from '@material-ui/core/Button';

interface ConfirmationOptions {
    maxWidth?: 'xs' | 'sm' | 'md' | 'lg' | false;
    fullWidth?: boolean;
    okLabel?: string;
    cancelLabel?: string;
    confirmation?: string;
    title?: string;
    onProceed?: () => void;
    onCancel?: () => void;
    component?: React.ReactElement
}

interface ConfirmationDialogProps extends ReactConfirmProps {
    options: ConfirmationOptions,
    ProceedButton?: React.Component<ButtonProps>
}

export class ConfirmationDialog extends React.Component<ConfirmationDialogProps> { }

export default function (confirmation?: string, options?: ConfirmationOptions): Promise;