import React from 'react';
import { Hidden, SvgIconProps } from '@material-ui/core';
import Mobile from './Mobile';
import Desktop from './Desktop';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface NavigatorMenuItem {
    label: string;
    url: string;
    requiredPermission?: string;
    IconProp?: IconProp;
}

interface DrawerNavigatorProps {
    menuList: NavigatorMenuItem[];
    open: boolean;
    onItemMenuClick?: (item: NavigatorMenuItem, closeMenu?: boolean) => any
}

export default class DrawerNavigator extends React.Component<DrawerNavigatorProps> {
    constructor(props: DrawerNavigatorProps) {
        super(props);
        this.onItemMenuClick = this.onItemMenuClick.bind(this);
    }

    onItemMenuClick(menuItem: NavigatorMenuItem, closeMenu?: boolean) {
        const { onItemMenuClick } = this.props;
        if (typeof onItemMenuClick === 'function') {
            onItemMenuClick(menuItem, closeMenu);
        }
    }

    render() {
        const { menuList, open: menuOpen } = this.props;
        return (
            <>
                <Hidden smDown >
                    <Desktop
                        menuList={menuList}
                        open={menuOpen}
                        onItemMenuClick={this.onItemMenuClick}
                    />
                </Hidden>
                <Hidden mdUp >
                    <Mobile
                        menuList={menuList}
                        open={menuOpen}
                        onItemMenuClick={this.onItemMenuClick}
                    />
                </Hidden>
            </>
        );
    }
}