import React from 'react';
import { withRouter } from "react-router";
import { Drawer, Tooltip, ListItem, List, withStyles } from '@material-ui/core';
import classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class DrawerNavigator extends React.Component {

    constructor(props) {
        super(props);
    }

    onItemMenuClick(menuItem) {
        const { onItemMenuClick } = this.props;
        if (typeof onItemMenuClick === 'function') {
            onItemMenuClick(menuItem);
        }
    }

    render() {
        const { menuList, open, classes } = this.props;
        return (
            <Drawer
                style={{ position: 'relative' }}
                classes={{
                    paper: classnames(classes.drawerPaper, (open && classes.drawerOpen), (!open && classes.drawerClose))
                }}
                variant='permanent'
            >
                <List>
                    {menuList.map((menuItem, index) => {
                        const { label, url, IconProp } = menuItem;
                        return (
                            <Tooltip key={index} title={label} placement="right">
                                <ListItem button key={index} onClick={() => this.onItemMenuClick(menuItem)}>
                                    {IconProp && <div style={{ width: 24, textAlign: 'center' }}>
                                        <FontAwesomeIcon size={'1x'} color='black' icon={IconProp} />
                                    </div>}
                                    {open && <span style={{ marginLeft: 8 }}>{label}</span>}
                                </ListItem>
                            </Tooltip>
                        )
                    })}
                </List>
            </Drawer>
        );
    }
}

export default withRouter(withStyles((theme) => ({
    drawerPaper: {
        position: 'unset'
    },
    drawerOpen: {
        width: 240,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        width: 56,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden'
    },
}))(DrawerNavigator));

