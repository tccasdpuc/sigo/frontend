import React from 'react';

import { NavigatorMenuItem } from '../';

export interface DrawerNavigatorProps {
    open: boolean;
    menuList: NavigatorMenuItem[];
    onItemMenuClick: (item: NavigatorMenuItem, closeMenu?: boolean) => any;
}

export default class DrawerNavigator extends React.Component<DrawerNavigatorProps> {

}