import React from 'react';
import { withRouter } from "react-router";
import { Drawer, ListItemIcon, ListItemText, ListItem, List, withStyles } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class DrawerNavigator extends React.Component {

    constructor(props) {
        super(props);
    }

    onItemMenuClick(menuItem) {
        const { onItemMenuClick } = this.props;
        if (typeof onItemMenuClick === 'function') {
            onItemMenuClick(menuItem, true);
        }
    }

    render() {
        const { menuList, open, classes } = this.props;
        return (
            <Drawer
                style={{ position: 'relative' }}
                classes={{
                    paper: classes.drawerPaper
                }}
                open={open}
                variant='persistent'
                PaperProps={{ elevation: 2 }}
            >
                <List>
                    {menuList.map((menuItem, index) => {
                        const { label, url, IconProp } = menuItem;
                        return (
                            <ListItem button key={index} onClick={() => this.onItemMenuClick(menuItem)}>
                                {IconProp && <div style={{ width: 24, textAlign: 'center' }}>
                                    <FontAwesomeIcon size={'sm'} color='black' icon={IconProp} />
                                </div>}
                                <span style={{ marginLeft: 8 }}>{label}</span>
                            </ListItem>
                        )
                    })}
                </List>
            </Drawer>
        );
    }
}

export default withRouter(withStyles((theme) => ({
    drawerPaper: {
        position: 'absolute',
        width: 240
    }
}))(DrawerNavigator));

