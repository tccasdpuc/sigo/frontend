import React, { RefObject } from 'react';

interface ColumnItem<T extends object> {
    field: keyof T;
    value?: string | React.Component;
    visible?: boolean;
    width?: number | string;
    alignContent?: 'left' | 'center' | 'right';
}

interface ValidationResult {
    error?: string;
    warning?: string;
}

interface TableListPsimProps<T extends Object> {
    title?: string;
    emptyRowsLabel?: string;
    rows: T[];
    columns: ColumnItem<T>[];

    page?: number;
    totalCount?: number;
    rowsPerPageOptions?: number[];
    rowsPerPage?: number;

    addLabel?: string;
    editLabel?: string;
    deleteLabel?: string;

    onRefreshClick?: () => void;
    onExportClick?: () => void;
    onFilterClick?: () => void;
    onSettingClick?: () => void;
    onAddClick?: () => void;
    onRowEdit?: (row: T) => void;
    onRowsDelete?: (rows: T[]) => void;
    onSearch?: (value: string) => void;
    onReorder?: (row: T, direction: number) => void;
    onPageChange?: (page: number) => void;
    onRowsPerPageChange?: (rowsPerPage: number) => void;
    onRowClick?: (row: T) => void;
    deleteValidation?: (rows: T[]) => Promise<ValidationResult | null>

    selectableRow?: boolean;

    noPadding?: boolean;
}

class TableListPsim<T> extends React.Component<TableListPsimProps<T>>{ }

export default TableListPsim;