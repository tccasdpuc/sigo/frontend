import React, { useState } from 'react';
import { IconButton, Tooltip } from '@material-ui/core'
import UpIcon from '@material-ui/icons/VerticalAlignTop';
import DownIcon from '@material-ui/icons/VerticalAlignBottom';
import SearchIcon from '@material-ui/icons/Search';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SettingsIcon from '@material-ui/icons/Settings';
import DownloadIcon from '@material-ui/icons/Unarchive';
import AddIcon from '@material-ui/icons/AddCircle';
import FilterIcon from '@material-ui/icons/FilterList';
import RefreshIcon from '@material-ui/icons/Autorenew';
import Search from '../Search/Search';

export default function Toolbar(props) {
    const [openSearch, setOpenSearch] = useState(false);
    const {
        onSearch,
        onReorder,
        onRefreshClick,
        onExportClick,
        onFilterClick,
        onSettingClick,
        onAddClick,
        selectedRows,
        onEditClick,
        onDeleteClick,
        disableReorderUp,
        disableReorderDown,
        addLabel, editLabel, deleteLabel
    } = props;

    var timerSearchText = null;

    function openSeach() {
        setOpenSearch(true);
    }

    function closeSeach() {
        setOpenSearch(false);
        onChangeSearchHandle("");
    }

    function onChangeSearchHandle(value) {
        clearTimeout(timerSearchText);
        timerSearchText = setTimeout(() => {
            if (typeof onSearch === 'function') {
                onSearch(value);
            }
        }, 400);
    }

    return (
        <>
            {typeof onSearch === 'function' &&
                <>
                    {openSearch &&
                        <Search handleOnChange={onChangeSearchHandle} onCancel={closeSeach} />
                    }
                    {!openSearch &&
                        <Tooltip title="Pesquisar">
                            <IconButton
                                size='small'
                                onClick={openSeach} color="inherit">
                                <SearchIcon />
                            </IconButton>
                        </Tooltip>
                    }
                </>
            }

            {(selectedRows === 1 && typeof onReorder === 'function') &&
                <>
                    {!disableReorderUp && <Tooltip title="Subir Item">
                        <IconButton
                            size='small'
                            onClick={() => onReorder(-1)} color="inherit">
                            <UpIcon />
                        </IconButton>
                    </Tooltip>}
                    {!disableReorderDown && <Tooltip title="Descer Item">
                        <IconButton
                            size='small'
                            onClick={() => onReorder(1)} color="inherit">
                            <DownIcon />
                        </IconButton>
                    </Tooltip>}
                </>
            }

            {typeof onExportClick === 'function' &&
                <Tooltip title="Exportar">
                    <IconButton
                        size='small'
                        onClick={onExportClick} color="inherit">
                        <DownloadIcon />
                    </IconButton>
                </Tooltip>
            }

            {typeof onFilterClick === 'function' &&
                <Tooltip title="Filtrar">
                    <IconButton
                        size='small'
                        onClick={onFilterClick} color="inherit">
                        <FilterIcon />
                    </IconButton>
                </Tooltip>
            }

            {typeof onSettingClick === 'function' &&
                <Tooltip title="Gerenciar">
                    <IconButton
                        size='small'
                        onClick={onSettingClick} color="inherit">
                        <SettingsIcon />
                    </IconButton>
                </Tooltip>
            }

            {typeof onAddClick === 'function' &&
                <Tooltip title={addLabel || "Adicionar"}>
                    <IconButton
                        size='small'
                        onClick={onAddClick} color="inherit">
                        <AddIcon />
                    </IconButton>
                </Tooltip>
            }

            {(selectedRows === 1 && typeof onEditClick === 'function') &&
                <Tooltip title={editLabel || "Editar"}>
                    <IconButton
                        size='small'
                        onClick={onEditClick} color="inherit">
                        <EditIcon />
                    </IconButton>
                </Tooltip>
            }

            {(selectedRows > 0 && typeof onDeleteClick === 'function') &&
                <Tooltip title={deleteLabel || "Deletar"}>
                    <IconButton
                        size='small'
                        onClick={onDeleteClick} color="inherit">
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            }

            {typeof onRefreshClick === 'function' &&
                <Tooltip title="Atualizar">
                    <IconButton
                        size='small'
                        onClick={onRefreshClick} color="inherit">
                        <RefreshIcon />
                    </IconButton>
                </Tooltip>
            }

        </>
    )
}