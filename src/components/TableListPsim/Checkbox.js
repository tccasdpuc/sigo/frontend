
import React from 'react';
import MuiCheckbox from '@material-ui/core/Checkbox';
import withStyles from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

/**
 * 
 * @param {Theme} theme 
 * @returns {{[key:string]:React.CSSProperties}}
 */
function style(theme) {
    return {
        root: {
            padding: 0,
            fontSize: '1.2rem'
        }
    }
};

export default withStyles(style, { name: 'Checkbox' })(MuiCheckbox)