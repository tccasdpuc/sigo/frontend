import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { Popper, Grow, MenuList, Paper, ClickAwayListener } from '@material-ui/core';
import { Link } from "react-router-dom";

export default function MobileMenuList({ open, anchorEl, onClickAway, menuList }) {
    return (
        <Popper open={open} anchorEl={anchorEl} transition disablePortal style={{ zIndex: 9999 }}>
            {({ TransitionProps, placement }) => (
                <Grow
                    {...TransitionProps}
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                    <Paper>
                        <ClickAwayListener onClickAway={onClickAway}>
                            <MenuList>
                                {menuList.map((item, i) => (
                                    <MenuItem key={i} selected={!!(item.url && String(window.location.href).includes(item.url))} onClick={onClickAway} to={item.url} component={Link}>
                                        <p>{item.label}</p>
                                    </MenuItem>
                                ))}
                            </MenuList>
                        </ClickAwayListener>
                    </Paper>
                </Grow>
            )}
        </Popper>
    )
}