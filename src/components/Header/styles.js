/**
 * 
 * @param {import("@material-ui/core").Theme} theme 
 */
const styles = theme => ({
    root: {
        width: '100%',
        '& p': {
            margin: 0
        }
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        textTransform: 'none',
        marginRight: theme.spacing(5),
    },
    header: {
        backgroundColor: '#ffffff',
        boxShadow: 'none',
        display: 'flex',
        // alignItems: 'center',
    },
    toolbar: {
        display: 'flex',
    },
    logo: {
        margin: theme.spacing(0, 2),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        //width: '25%',
        [theme.breakpoints.up('md')]: {
            //width: '15%',
        },
    },
    menuItem: {
        height: 48
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    sectionDesktop: {
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
    },
    sectionMobile: {
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    fontSize35: {
        fontSize: 35
    },
    avatar: {
        margin: theme.spacing(1),
    },
    menuContent: {
        minWidth: '200px',
        padding: theme.spacing(2),
    }
});

export default styles;