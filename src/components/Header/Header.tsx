
import React from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import UserIcon from '@material-ui/icons/Person';
import { withStyles, Theme } from '@material-ui/core';
import { logoutAction, AuthenticationState } from "../../reducer/authentication";
import AuthService from "../../service/AuthService";
import { loadingAction } from "./../../reducer/loading";
import Logo from '../../images/logo.png';
import UserMenu from './UserMenu';
import { RootState } from '../../reducer';

export interface HeaderProps extends AuthenticationState {
    onMenuClick: () => void,
    loadingAction: typeof loadingAction,
    logoutAction: typeof logoutAction,
    classes: Record<"root" | "header" | "toolbar" | "logo" | "headerCenter", string>
}


class Header extends React.Component<HeaderProps> {

    constructor(props: HeaderProps) {
        super(props);
        this.logOut = this.logOut.bind(this);
    }

    async logOut() {
        const { loadingAction, logoutAction } = this.props;
        loadingAction(true);
        await AuthService.instance.logout();
        loadingAction(false);
        logoutAction();
    }

    render() {
        const { classes, onMenuClick } = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="static" className={classes.header}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton aria-haspopup="true" onClick={onMenuClick} color="default">
                            <MenuIcon />
                        </IconButton>
                        <div className={classes.logo}>
                            <img alt={'SIGO'} src={Logo} style={{ height: 48 }} />
                        </div>

                        <div className={''} style={{ flex: 1 }}>
                            <div className={classes.headerCenter}>

                            </div>
                        </div>
                        <IconButton
                            onClick={() => this.logOut()}
                        >
                            <UserIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}


function mapStateToProps(state: RootState) {
    return state.authentication;
}

const mapDispatchToProps = {
    loadingAction,
    logoutAction
};

export default withStyles((theme: Theme) => ({
    root: {
        width: '100%',
        '& p': {
            margin: 0
        }
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        textTransform: 'none',
        marginRight: theme.spacing(5),
    },
    header: {
        backgroundColor: '#ffffff',
        boxShadow: 'none',
        display: 'flex',
        // alignItems: 'center',
    },
    toolbar: {
        display: 'flex',
    },
    logo: {
        margin: theme.spacing(0, 2),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        //width: '25%',
        [theme.breakpoints.up('md')]: {
            //width: '15%',
        },
    },
    menuItem: {
        height: 48
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    sectionDesktop: {
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
    },
    sectionMobile: {
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    fontSize35: {
        fontSize: 35
    },
    avatar: {
        margin: theme.spacing(1),
    },
    menuContent: {
        minWidth: '200px',
        padding: theme.spacing(2),
    },
    headerCenter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        color: theme.palette.text.primary
    }
}))(connect(mapStateToProps, mapDispatchToProps)(Header));