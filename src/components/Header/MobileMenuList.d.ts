import React from 'react';
import { Popper } from '@material-ui/core';
import { HeaderMenuItem } from './Header';

export interface MobileMenuListProps {
    open: boolean;
    anchorEl: any;
    onClickAway: (event: React.MouseEvent<Document>) => void;
    menuList: HeaderMenuItem[];
}

const MobileMenuList: React.FunctionComponent<MobileMenuListProps>;

export default MobileMenuList;