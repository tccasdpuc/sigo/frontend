import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import UserIcon from '@material-ui/icons/Person';
import ExitToApp from '@material-ui/icons/ExitToApp';
import PermIdentity from '@material-ui/icons/PermIdentity';
import { Typography, Popper, Grow, MenuList, Paper, ClickAwayListener, Avatar, Grid, Hidden, IconButton, withStyles } from '@material-ui/core';

import styles from './styles';

const UserPopperMenu = withStyles(styles)(({ user, open, anchorEl, onClickAway, onLogout, classes }) => (
    <Popper open={open} anchorEl={anchorEl} transition disablePortal /*style={{ zIndex: 9999 }}*/>
        {({ TransitionProps, placement }) => (
            <Grow
                {...TransitionProps}
                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
                <ClickAwayListener onClickAway={onClickAway}>
                    <Paper className={classes.menuContent}>
                        <>
                            <Grid container justify="center" alignItems="center">
                                <Avatar className={classes.avatar}>
                                    <UserIcon />
                                </Avatar>
                            </Grid>
                            <Grid container justify="center" alignItems="center">
                                <Typography align='center'>{user.name}</Typography>
                            </Grid>

                            <MenuList>
                                <MenuItem onClick={onLogout}><ExitToApp /> Sair</MenuItem>
                            </MenuList>
                        </>
                    </Paper>
                </ClickAwayListener>
            </Grow>
        )}
    </Popper>
));


class UserMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            open: false
        }
        this.closeMenu = this.closeMenu.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu(event) {
        const { open } = this.state;
        this.setState({
            anchorEl: open ? null : event.currentTarget,
            open: !open
        });
    }

    closeMenu() {
        this.setState({
            anchorEl: null,
            open: false
        });
    }

    render() {
        const { user, classes, onLogout } = this.props;
        const { anchorEl, open } = this.state;
        return (
            <>
                <div
                    style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', cursor: 'pointer' }}
                    onClick={this.toggleMenu}
                >
                    <IconButton
                        aria-owns={open ? 'material-appbar' : undefined}
                        aria-haspopup="true"
                        color="default"
                    >
                        <PermIdentity className={classes.UserIcon} />
                    </IconButton>
                    <Hidden mdDown>
                        <Typography color='textPrimary'>{user.name}</Typography>
                    </Hidden>
                </div>
                <UserPopperMenu user={user} open={open} onClickAway={this.closeMenu} anchorEl={anchorEl} onLogout={onLogout} />
            </>
        )
    }
}

export default withStyles((theme) => ({
    'UserIcon': {
        fontSize: 35,
        [theme.breakpoints.down('md')]: {
            fontSize: 24,
        },
    }
}))(UserMenu);