import React from 'react';
import { Popper } from '@material-ui/core';
import { HeaderMenuItem } from './Header';
import Operator from '../../model/Operator';

export interface UserMenuProps {   
    user: Operator;
    onLogout: () => void;
}

const UserMenu: React.FunctionComponent<UserMenuProps>;

export default UserMenu;