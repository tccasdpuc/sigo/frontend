import React from 'react';
import PropTypes from 'prop-types';
import { confirmable, createConfirmation } from 'react-confirm';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { MuiThemeProvider } from '@material-ui/core/styles';
import ErrorButton from './FormControl/ErrorButton';
import SuccessButton from './FormControl/SuccessButton';
import { ThemeBlue } from "../themes/ThemeBlue";

export class ConfirmationDialog extends React.Component {

    static propTypes = {
        show: PropTypes.bool,            // from confirmable. indicates if the dialog is shown or not.
        proceed: PropTypes.func,         // from confirmable. call to close the dialog with promise resolved.
        cancel: PropTypes.func,          // from confirmable. call to close the dialog with promise rejected.
        dismiss: PropTypes.func,         // from confirmable. call to only close the dialog.
        confirmation: PropTypes.string,
        options: PropTypes.shape({
            okLabel: PropTypes.string,
            cancelLabel: PropTypes.string,
            title: PropTypes.string,
        }),
    }

    render() {
        const { show, proceed, dismiss, cancel, confirmation, options, children, ProceedButton } = this.props;
        const { maxWidth, fullWidth, okLabel, cancelLabel, title, onProceed, onCancel, component, disableBackdropClick, disableEscapeKeyDown } = options || {};
        const OkButton = ProceedButton || SuccessButton;
        return (
            <MuiThemeProvider theme={ThemeBlue}>
                <Dialog
                    open={show}
                    aria-labelledby="responsive-dialog-title"
                    onClose={dismiss}
                    maxWidth={maxWidth || null}
                    fullWidth={fullWidth || false}
                    disableBackdropClick={disableBackdropClick || false}
                    disableEscapeKeyDown={disableEscapeKeyDown || false}
                    PaperProps={{
                        style: {
                            overflowY: 'unset'
                        }
                    }}
                >
                    {title && <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>}
                    <DialogContent style={{ overflowY: 'unset' }}>
                        {confirmation ?
                            <DialogContentText>
                                {confirmation}
                            </DialogContentText> :
                            (component || children)}
                    </DialogContent>
                    <DialogActions>
                        <OkButton
                            size='small'
                            variant='contained'
                            onClick={async () => {
                                if (typeof onProceed === 'function') {
                                    if (await Promise.resolve(onProceed()) === false) {
                                        return;
                                    }
                                }
                                if (typeof proceed === 'function') {
                                    proceed();
                                }
                            }}
                            autoFocus>
                            {okLabel || 'OK'}
                        </OkButton>
                        <ErrorButton
                            size='small'
                            variant='contained'
                            onClick={() => {
                                if (typeof onCancel === 'function') {
                                    onCancel();
                                }
                                if (typeof cancel === 'function') {
                                    cancel();
                                }
                            }} >
                            {cancelLabel || 'Cancelar'}
                        </ErrorButton>
                    </DialogActions>
                </Dialog>
            </MuiThemeProvider>
        );
    }
}

const ConfirmableDialog = confirmable(ConfirmationDialog);

const confirm = createConfirmation(ConfirmableDialog, 400, document.getSelection('.App').anchorNode);

export default function (confirmation, options = {}) {
    // You can pass whatever you want to the component. These arguments will be your Component's props
    return confirm({ confirmation, options });
}