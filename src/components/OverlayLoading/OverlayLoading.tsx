import React from "react";
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { mapLoadingStateToProps } from "../../reducer/loading";
import { withStyles } from "@material-ui/core";

interface CircularIndeterminateProps {
    loading: boolean;
    classes: Record<"progress" | "overlay", string>
}

function CircularIndeterminate(props: CircularIndeterminateProps) {
    const { classes, loading } = props;
    return loading ? (
        <div className={classes.overlay}>
            <CircularProgress className={classes.progress} size={50} />
        </div>
    ) : null;
}

export const OverlayLoading = withStyles((theme) => ({
    progress: {
        margin: theme.spacing(2),
    },
    overlay: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        zIndex: 100000
    }
}))(CircularIndeterminate);

export default connect(mapLoadingStateToProps)(OverlayLoading);