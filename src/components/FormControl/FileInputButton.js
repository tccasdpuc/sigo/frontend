import React from "react";
import PropTypes from 'prop-types';
import { Button } from "@material-ui/core";

export class FileInputButton extends React.PureComponent {

    static propTypes = {
        onFileChange: PropTypes.func,
        accept: PropTypes.string,
        color: PropTypes.oneOf([
            'default',
            'inherit',
            'primary',
            'secondary'
        ]),
        variant: PropTypes.oneOf([
            'text',
            'flat',
            'outlined',
            'contained',
            'raised',
            'fab',
            'extendedFab'
        ]),
        size: PropTypes.oneOf([
            'small',
            'medium',
            'large'
        ]),
        disabled: PropTypes.bool
    };

    constructor(props) {
        super(props);
        this.refInputFile = null;
        this.buttonClickHandler = this.buttonClickHandler.bind(this);
        this.inputChangeHandler = this.inputChangeHandler.bind(this);
    }

    inputChangeHandler(e) {
        const { onFileChange } = this.props
        if (this.refInputFile.files.length > 0) {
            var file = this.refInputFile.files[0];
            if (typeof onFileChange === 'function') {
                onFileChange(file);
            }
        }
    }

    buttonClickHandler(e) {
        this.refInputFile.click();
    }

    render() {
        const {
            accept,
            children,
            color,
            style,
            className,
            disabled,
            variant,
            size
        } = this.props;
        return (
            <Button
                onClick={this.buttonClickHandler}
                color={color}
                style={style}
                className={className}
                disabled={disabled}
                variant={variant}
                size={size}
            >
                <input
                    type='file'
                    ref={_ref => this.refInputFile = _ref}
                    style={{ display: 'none' }}
                    onChange={this.inputChangeHandler}
                    multiple={false}
                    accept={accept}
                />
                {children}
            </Button>
        );
    }
};
