import { Button, withStyles } from "@material-ui/core";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { green } from "./../../themes/colors";
export default withStyles(theme => ({
    text: {
        color: green.main,
        '&:hover': {
            backgroundColor: fade(green.main, theme.palette.action.hoverOpacity),
        }
    },
    outlined: {
        color: green.main,
        border: `1px solid ${fade(green.main, 0.5)}`,
        '&:hover': {
            border: `1px solid ${green.main}`,
            backgroundColor: fade(green.main, theme.palette.action.hoverOpacity),
        }
    },
    contained: {
        color: green.contrastText,
        backgroundColor: green.main,
        '&:hover': {
            backgroundColor: green.dark
        },
        '&:disabled': {
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            color: '#999999'
        },
    }
}), { name: 'MuiButton-Success' })(Button);