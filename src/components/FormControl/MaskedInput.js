import React from 'react';
import { conformToMask } from 'react-text-mask'
import TextField from '@material-ui/core/TextField';

function MaskedInput({ mask, value, ...props }) {    
    return (
        <TextField
            {...props}
            value={conformToMask(value, mask, { placeholderChar: '\u2000', guide: false }).conformedValue}
        />
    );
}

export default MaskedInput;