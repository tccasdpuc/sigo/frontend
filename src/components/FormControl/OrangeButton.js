import { Button, withStyles } from "@material-ui/core";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { orange } from "../../themes/colors";
export default withStyles(theme => ({
    text: {
        color: orange.main,
        '&:hover': {
            backgroundColor: fade(orange.main, theme.palette.action.hoverOpacity),
        }
    },
    outlined: {
        color: orange.main,
        border: `1px solid ${fade(orange.main, 0.5)}`,
        '&:hover': {
            border: `1px solid ${orange.main}`,
            backgroundColor: fade(orange.main, theme.palette.action.hoverOpacity),
        }
    },
    contained: {
        color: orange.contrastText,
        backgroundColor: orange.main,
        '&:hover': {
            backgroundColor: orange.dark
        },
        '&:disabled': {
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            color: '#999999'
        },
    }
}), { name: 'MuiButton-Orange' })(Button);