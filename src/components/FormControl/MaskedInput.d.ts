import React from 'react';
import { TextFieldProps, OutlinedTextFieldProps } from "@material-ui/core/TextField";

type MaskArray = Array<string | RegExp>;

interface MaskedInputProps extends OutlinedTextFieldProps {
    mask?: MaskArray | ((value: string) => MaskArray);
}
const MaskedInput: React.FunctionComponent<MaskedInputProps>;

export default MaskedInput;