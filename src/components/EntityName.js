import React from 'react';
import { SigoApi } from "../service/SigoApi";

export default class EntityName extends React.Component {

    constructor(props) {
        super(props);
        const { entity } = props;
        this.state = {
            name: null
        }
        this.entityApi = new SigoApi(entity);
    }

    componentDidMount() {
        this.getName();
    }

    componentDidUpdate(prevProps) {
        const { entityId } = this.props;
        if (entityId !== prevProps.entityId) {
            this.getName();
        }
    }

    async getName() {
        const { entityId, propName } = this.props;
        const prop = propName || 'name';
        const entity = await this.entityApi.getById(entityId, { select: [prop] });
        if (entity) {
            this.setState({ name: entity[prop] });
        }
    }

    render() {
        return this.state.name;
    }
}