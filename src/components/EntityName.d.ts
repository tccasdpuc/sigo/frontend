import React from 'react';

interface EntityNameProps {
    entity: string;
    entityId: number | string;
    propName?: string;
}

export default class EntityName extends React.Component<EntityNameProps> {
}