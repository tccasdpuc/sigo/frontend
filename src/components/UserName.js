import React from 'react';
import EntityName from './EntityName';

export default function UserName(props) {
    return <EntityName entity='user' entityId={props.userGuid} />
}