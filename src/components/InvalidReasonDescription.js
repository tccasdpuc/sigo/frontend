import React from 'react';
import EntityName from './EntityName';

export default function InvalidReasonDescription(props) {
    return <EntityName entity='invalidReason' entityId={props.invalidReasonCode} propName='description' />
}