const styles = theme => ({
    root: {
        height: '100%',
        overflowY: 'auto',
        backgroundColor: theme.palette.background.paper,
    },
    subheader: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontWeight: 'bold'
    },
    parentLink: {
        paddingTop: 3,
        paddingBottom: 3
    },
    nested: {
        paddingTop: 3,
        paddingBottom: 3
    },
    nestedIcon: {
        marginRight: 0,
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        marginRight: theme.spacing(2),
        marginLeft: 0,
        marginTop: 0,
        marginBottom: 0,
        width: '100%',
    },
    searchIcon: {
        width: theme.spacing(6),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        border: '1px solid #7a7a7a',
        borderRadius: 20,
        marginLeft: 0
    },
    inputInput: {
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        paddingLeft: theme.spacing(6),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    expand: {
        '&:hover': {
            color: 'blue'
        }
    }
});

export default styles;