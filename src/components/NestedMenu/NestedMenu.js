import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import styles from './styles';
import { Tooltip } from '@material-ui/core';

const ListItemTextRoot = { padding: '0px 5px' };
const nestedSpacing = 10;

class NestedMenu extends React.Component {
    render() {
        const { classes, list, onClickHandle, createOpen } = this.props;
        const listItems = list.map((item, i) => (
            <MenuNestedCollapseItem key={i} menu={item} classes={classes} onClickHandle={onClickHandle} createOpen={createOpen} />
        ));
        return (
            <List>{listItems}</List>
        );
    }
}
class MenuNestedCollapse extends React.Component {
    render() {
        const { menu, classes, open, onClickHandle, createOpen } = this.props;
        return (
            <Collapse in={open} timeout="auto" unmountOnExit>
                <div style={{ display: 'flex' }} >
                    <div style={{ width: nestedSpacing }} ></div>
                    <List component="div" disablePadding style={{ flex: 1 }}>
                        {menu.subs.map((m, i) => (<MenuNestedCollapseItem key={i} menu={m} classes={classes} onClickHandle={onClickHandle} createOpen={createOpen} />))}
                    </List>
                </div>
            </Collapse>
        );
    }
}
class MenuNestedCollapseItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.createOpen,
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { open } = this.state;
        this.setState({
            open: !open
        });
    }

    render() {
        const { menu, classes, onClickHandle, createOpen } = this.props;
        const { open } = this.state;
        const Expand = open ? ExpandLess : ExpandMore;
        const url = window.location.hash;
        return (
            <Fragment>
                <ListItem selected={!!(menu.id && String(url).includes(menu.id))} button className={classes.nested}>
                    {
                        menu.icon ?
                            < ListItemIcon className={classes.nestedIcon}>
                                <menu.icon style={{ fontSize: menu.subs.length ? 24 : 18 }} />
                            </ListItemIcon>
                            : null
                    }
                    <ListItemText primary={menu.label} style={ListItemTextRoot} onClick={() => onClickHandle(menu.id)} />
                    {menu.subs.length ? (
                        <Tooltip title="Expandir/Ocultar" aria-label="Expandir/Ocultar">
                            <Expand className={classes.expand} onClick={this.handleClick} />
                        </Tooltip>
                    ) : (null)}
                </ListItem>
                {
                    menu.subs.length && open ?
                        <MenuNestedCollapse menu={menu} classes={classes} open={open} onClickHandle={onClickHandle} createOpen={createOpen} />
                        : null
                }
            </Fragment >
        );
    }
}

NestedMenu.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NestedMenu);