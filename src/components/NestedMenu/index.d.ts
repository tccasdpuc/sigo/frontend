import React from 'react';

interface MenuItem {
    id: any;
    label: string;
    icon?: any;
    subs?: MenuItem[];
}

interface NestedMenuProps {
    list: MenuItem[];
    createOpen?: Boolean;
    onClickHandle: (id: any) => void;
}

class NestedMenu extends React.Component<NestedMenuProps>{ }

export default NestedMenu;