import React from 'react';

interface SearchProps {
  handleOnChange: () => void
  onCancel?: () => void
}

class Search extends React.Component<SearchProps>{ }
export default Search