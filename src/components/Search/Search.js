import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import CancelIcon from '@material-ui/icons/Close';
import { InputAdornment, IconButton } from '@material-ui/core';
import styles from './styles';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeHandle = this.onChangeHandle.bind(this);
        this.escFunction = this.escFunction.bind(this);
    }

    onChangeHandle(event) {
        const { handleOnChange } = this.props;
        if (typeof handleOnChange === 'function') handleOnChange(event.target.value)
    }

    escFunction(event) {
        const { onCancel } = this.props;
        if (event.keyCode === 27) {
            if (typeof onCancel === 'function') onCancel();
        }
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    render() {
        const { classes, onCancel } = this.props;

        return (
            <div className={classes.search}>

                <InputBase
                    autoFocus={true}
                    placeholder="Busca"
                    onChange={(e) => this.onChangeHandle(e)}
                    margin='dense'
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    }}
                    startAdornment={
                        <SearchIcon />
                    }
                    endAdornment={
                        typeof onCancel === 'function' &&
                        (
                            <InputAdornment position="end">
                                <IconButton
                                    size='small'
                                    onClick={onCancel} >
                                    <CancelIcon />
                                </IconButton>
                            </InputAdornment>
                        )
                    }
                />
            </div>
        );
    }
}

export default withStyles(styles)(Search);