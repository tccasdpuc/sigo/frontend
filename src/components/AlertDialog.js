import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { mapAlertDialogStateToProps, closeAlertDialog } from '../reducer/alertDialog';

class AlertDialog extends React.Component {

    constructor(props) {
        super(props);
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick(e) {
        const { closeAlertDialog } = this.props;
        closeAlertDialog();
    }

    render() {
        const { alertTitle, alertContent, alertOpen } = this.props;
        return (
            <Dialog
                open={alertOpen}
                aria-labelledby="responsive-dialog-title"
            >
                {alertTitle && <DialogTitle id="responsive-dialog-title">{alertTitle}</DialogTitle>}
                <DialogContent>
                    <DialogContentText style={{ minWidth: 320 }}>
                        {alertContent}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button size='small' onClick={this.handleButtonClick} color="primary" variant='contained' autoFocus>Ok</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default connect(mapAlertDialogStateToProps, { closeAlertDialog })(AlertDialog);