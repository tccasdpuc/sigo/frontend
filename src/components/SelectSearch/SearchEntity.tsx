import React from 'react';
import { SigoApi, WhereCondition } from '../../service/SigoApi';
import { GuidIdentityEntity } from '../../model';
import SelectSearchAsync from './SelectSearchAsync';
import OptionItem from './OptionItem';

interface SearchEntityProps<T> {
    name: string;
    label: string;
    field: keyof T;
    entity: string;
    value?: T,
    onChangeHandle: (name: string, value: T | null) => any;
    defaultOptions?: T[];
}

interface SearchEntityState<T> {

}

class SearchEntity<T extends GuidIdentityEntity> extends React.Component<SearchEntityProps<T>, SearchEntityState<T>> {

    private api: SigoApi<T>;

    constructor(props: SearchEntityProps<T>) {
        super(props);
        this.api = new SigoApi<T>(props.entity);
        this.loadOptions = this.loadOptions.bind(this);
        this.mapOptionItem = this.mapOptionItem.bind(this);
    }

    async loadOptions(text: string): Promise<OptionItem<T>[]> {
        const where: WhereCondition<T> = [];
        if (text) {
            const { field } = this.props;
            where.push([field, 'like', `%${text}%`]);
        }
        const { data } = await this.api.getList({
            pagination: true,
            offset: 0,
            limit: 100,
            where
        });
        return data.map(this.mapOptionItem);
    }

    mapOptionItem(i: T): OptionItem<T> {
        const { field } = this.props;
        return {
            label: String(i[field]),
            value: i
        };
    }

    render() {
        const { name, label, field, onChangeHandle, value, defaultOptions } = this.props;
        return (
            <SelectSearchAsync<T>
                name={name}
                label={label}
                loadOptions={this.loadOptions}
                onChangeHandle={(name, value) => onChangeHandle(name, value || null)}
                value={value ? value : undefined}
                defaultOptions={defaultOptions ? defaultOptions.map(this.mapOptionItem) : undefined}
                margin="dense"
                fullWidth
            />
        );
    }

}

export default SearchEntity;