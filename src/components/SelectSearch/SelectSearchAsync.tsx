import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { objectEquals, delay } from '../../utils';
import OptionItem from './OptionItem';

interface SelectSearchAsyncProps<T> {
    id?: string | undefined;
    name: string;
    label: String;
    loadOptions: (inputValue: string) => Promise<OptionItem<T>[]>;
    defaultOptions?: OptionItem<T>[];
    //isMulti?: Boolean;
    isClearable?: boolean;
    value?: T | null;
    isDisabled?: Boolean;
    onChangeHandle: (name: string, value: T | null) => void;
    error?: Boolean;
    required?: Boolean;
    helperText?: String;
    placeholder?: string;
    margin?: 'none' | 'dense' | 'normal';
    //variant?: 'standard' | 'filled' | 'outlined';
    fullWidth?: boolean;
    //classes: Record<keyof ReturnType<typeof styles>, string>
}

interface SelectSearchAsyncState<T> {
    options: OptionItem<T>[];
    loading: boolean;
}

class SelectSearchAsync<T> extends React.Component<SelectSearchAsyncProps<T>, SelectSearchAsyncState<T>>{

    timer?: NodeJS.Timer;

    constructor(props: SelectSearchAsyncProps<T>) {
        super(props);
        const { defaultOptions, value } = this.props;
        this.state = {
            options: defaultOptions || [],
            loading: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
    }

    private handleChange(e: React.ChangeEvent<any>, value: T | null) {
        const { onChangeHandle, name } = this.props;
        if (typeof onChangeHandle === 'function') {
            onChangeHandle(name, value);
        }
    }

    private async loadOptions(text: string) {
        const { loadOptions } = this.props;
        this.setState({ loading: true, options: [] });
        const options = await loadOptions(text);
        this.setState({ loading: false, options });
    }

    private async onTextChange(text: string, reason: 'input' | 'reset' | 'clear') {
        const { name, onChangeHandle } = this.props;
        if (this.timer) {
            clearTimeout(this.timer);
        }
        if (reason === 'input') {
            this.timer = setTimeout(async () => {
                await this.loadOptions(text);
            }, 300);
        } else if (reason === 'clear') {
            if (typeof onChangeHandle === 'function') {
                onChangeHandle(name, null);
            }
            await this.loadOptions(text);
        }
    }

    render() {
        var { name, label, id, error, helperText, required, value, isDisabled, isClearable, margin, fullWidth, placeholder } = this.props;
        const { options, loading } = this.state;

        var disableClearable = !isClearable || !!required;
        return (
            <Autocomplete<T>
                id={id}
                options={options.map(({ value }) => value)}
                loading={loading}
                value={value}
                getOptionSelected={(a, b) => objectEquals(a, b)}                
                //noOptionsText={loading ? 'Loading...' : undefined}
                getOptionLabel={v => {
                    const option = options?.find(({ value }) => objectEquals(v, value));
                    return option?.label || '';
                }}
                onChange={this.handleChange}
                onInputChange={(e, value, reason) => this.onTextChange(value, reason)}
                //disableClearable={!isClearable || !!required}
                disabled={!!isDisabled}
                placeholder={placeholder}
                autoComplete
                renderInput={(params) => (
                    <TextField
                        {...params}
                        name={name}
                        label={label}
                        margin={margin || "normal"}
                        variant='outlined'
                        error={!!error}
                        helperText={helperText}
                        fullWidth={!!fullWidth}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            ),
                        }}
                    />
                )}
            />
        );
    }
}

export default SelectSearchAsync;