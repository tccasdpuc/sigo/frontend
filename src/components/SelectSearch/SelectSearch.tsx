import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { objectEquals } from '../../utils';
import OptionItem from './OptionItem';

interface SelectSearchProps<T> {
    id?: string | undefined;
    name: string;
    label: React.ReactNode;
    options?: OptionItem<T>[];
    //isMulti?: Boolean;
    isClearable?: boolean;
    value?: T | null;
    isDisabled?: Boolean;
    onChangeHandle: (name: string, value: T | null) => void;
    error?: boolean;
    required?: boolean;
    helperText?: string;
    placeholder?: string;
    margin?: 'none' | 'dense' | 'normal';
    //variant?: 'standard' | 'filled' | 'outlined';
    fullWidth?: boolean;
    //classes: Record<keyof ReturnType<typeof styles>, string>
}

class SelectSearch<T> extends React.Component<SelectSearchProps<T>> {

    constructor(props: SelectSearchProps<T>) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e: React.ChangeEvent<any>, value: OptionItem<T> | null) {
        const { onChangeHandle, name } = this.props;
        if (typeof onChangeHandle === 'function') {
            onChangeHandle(name, value ? value.value : null);
        }
    }

    render() {
        var { options, name, label, id, error, helperText, required, value, isDisabled, isClearable, margin, fullWidth, placeholder } = this.props;

        var disableClearable = !isClearable || required;

        return (
            <Autocomplete<OptionItem<T>>
                id={id}
                options={options || []}
                value={options?.find(({ value: v }) => v && value && objectEquals(v, value)) || null}
                getOptionSelected={(option, value) => objectEquals(option, value)}                
                //noOptionsText={loading ? 'Loading...' : undefined}
                getOptionLabel={v => v.label}
                //disableClearable={disableClearable}
                onChange={this.handleChange}
                disabled={!!isDisabled}
                placeholder={placeholder}
                autoComplete
                renderInput={(params) => (
                    <TextField
                        {...params}
                        name={name}
                        label={label}
                        margin={margin || "normal"}
                        variant='outlined'
                        error={!!error}
                        helperText={helperText}
                        fullWidth={!!fullWidth}
                    />
                )}
            />
        );
    }
}

export default SelectSearch;