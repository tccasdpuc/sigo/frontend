import { createMuiTheme } from '@material-ui/core/styles';

//http://paletton.com/#uid=43u0I0kKQrYrNFhHRDiNQmIRwh3
//primary: #045B8E
//secondary1: #DFB500
//secondary2: #D90010
//complementary: #DF7B00

export const ThemeBlue = createMuiTheme({
    spacing: 4,
    palette: {
        type: 'light',
        primary: {
            main: '#045a8e'
        },
        secondary: {
            main: '#DFB500'
        },
        error: {
            main: '#D90010'
        },
    },
    typography: {

        // fontFamily: [
        //     'Roboto Regular',
        //     '"Segoe UI"',
        // ].join(',')
    },
    overrides: {
        MUIDataTable: {
            root: {
                color: '#031527 !important',
            },
        },
        MUIDataTableBodyRow: {
            root: {
                '&:nth-child(even)': {
                    backgroundColor: 'rgba(172,235,238,0.4)'
                }
            },
        },
        MuiTableRow: {
            root: {
                height: 32,
                '&:nth-child(even)': {
                    backgroundColor: 'rgba(172,235,238,0.4)'
                },
            },
            head: {
                height: 32,
                backgroundImage: 'linear-gradient(#f0f3f4, #d5e0e4)',
            },
            hover: {
                '&:hover': {
                    cursor: 'pointer'
                }
            }
        },
        MUIDataTableHeadCell: {
            root: {
                fontSize: 16,
                color: '#031527 !important',
                textAlign: 'center !important',
                fontWeight: 'bold !important',
            },
        },
        MuiTableCell: {
            root: {
                borderBottom: undefined,
                border: '1px solid #031527',
                padding: `${4}px ${8}px`,
                '&:last-child': {
                    paddingRight: undefined
                },
            },
            head: {
                color: 'rgba(0, 0, 0, 0.87) !important',
                fontSize: 16,
            }
        },
        MuiTablePagination: {
            toolbar: {
                minHeight: 32,
                height: 32,
                paddingRight: 8
            },
            selectRoot: {
                marginRight: 24
            },
            selectIcon: {
                //fontSize: '1.2rem',
                //position: 'relative',
                //left: -20
                top: undefined
            },
            select: {
                //paddingRight: 16
            }
        },
        MuiMenuItem: {
            root: {
                lineHeight: undefined,
                minHeight: 32
            }
        },
        MuiList: {
            padding: {
                paddingTop: 4,
                paddingBottom: 4,
            }
        },
        MuiInputBase: {
            inputMarginDense: {
                fontSize: 14
            },
            marginDense: {
                padding: 0
            }
        },
        MuiFormControl: {
            marginDense: {
                marginTop: 4,
                marginBottom: 4
            }
        },
        MuiInputLabel: {
            marginDense: {

            },
            outlined: {
                '&$marginDense': {
                    transform: 'translate(8px, 9px) scale(1)',
                    fontSize: 14,
                    color: 'gray'
                },
                '&$shrink': {
                    color: 'unset'
                }
            }
        },
        MuiOutlinedInput: {
            root: {
                borderRadius: 0,
                '&$focused $notchedOutline': {
                    borderColor: '#031527'
                },
                '& $notchedOutline': {
                    borderColor: '#031527'
                }
            },
            inputMarginDense: {
                paddingTop: 8,
                paddingBottom: 8,
                paddingLeft: 8,
                paddingRight: 8
            }
        },
        MuiAutocomplete: {
            inputRoot: {
                '&[class*="MuiOutlinedInput-root"][class*="MuiOutlinedInput-marginDense"]': {
                    padding: 4,
                    '& $input': {
                        padding: '4px 4px',
                    },
                },
            }
        },
        PrivateNotchedOutline: {
            root: {
                borderRadius: 1
            }
        },
        MuiFormLabel: {
            root: {
                '&:not($error)': {
                    color: '#031527'
                }
            }
        },
        MuiListItem: {
            root: {
                color: 'rgba(0, 0, 0, 0.87)',
                "&$selected": {
                    backgroundColor: '#cfebff !important',
                    fontWeight: 600
                }
            },
        },
        MuiTabs: {
            root: {
                minHeight: 32,
            }
        },
        MuiTab: {
            root: {
                textTransform: 'none',
                minHeight: 32,
                '@media (min-width: 960px)': {
                    minWidth: 100
                },
                padding: '4px 8px'
            }
        },
        MuiSelect: {
            selectMenu: {
                display: 'flex',
                alignItems: 'center'
            },
            icon: {
                fontSize: '1.2rem',
                position: 'absolute',
                right: 8,
                top: undefined,
                color: 'rgba(0, 0, 0, 0.54)',
                cursor: 'pointer',
                '&:hover': {
                    color: 'inherit',
                }
            }
        },
        MuiStepLabel: {
            iconContainer: {
                fontSize: '1.2rem'
            }
        },
        MuiStepper: {
            root: {
                padding: 16
            }
        },
        MuiSvgIcon: {
            root: {
                fontSize: undefined
            },
            fontSizeSmall: {
                fontSize: '1rem'
            }
        },
        MuiTypography: {
            h6: {
                fontSize: '1rem',
                lineHeight: 1.5
            }
        },
        MuiButton: {
            sizeSmall: {
                fontSize: '0.7rem'
            }
        },
        MuiDialogTitle: {
            root: {
                padding: '16px 16px 8px 16px'
            }
        },
        MuiDialogContent: {
            root: {
                padding: '8px 16px'
            }
        },
        MuiExpansionPanelSummary: {
            root: {
                minHeight: 32
            }
        },
        MuiChip: {
            root: {
                height: 24
            }
        }
    }
});