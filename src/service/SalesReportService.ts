import { store } from "..";
import { Sales } from "../model/Sales";
import { loadingAction } from "../reducer/loading";
import { getBaseUrl, promiseRequest } from "./SigoApi";

export default class SalesReportService {


    constructor(private loading: boolean = true) {

    }

    private async promiseWithLoading<P>(promise: Promise<P>): Promise<P> {
        var result = null;
        try {
            this.loading && store.dispatch(loadingAction(true));
            result = await promise;
        } finally {
            this.loading && store.dispatch(loadingAction(false));
        }
        return result;
    }

    async getReport(year: number, month: number): Promise<Sales[]> {
        const baseUrl = getBaseUrl('esb/salesReport');
        const url = `${baseUrl}${year}/${month}`;
        return this.promiseWithLoading(promiseRequest(url));
    }


}