import { SigoApi, ConditionsOptions, FindManyOptions, FindOneOptions, GetOptions, PaginationFindManyOptions, PaginationResult } from "./SigoApi";
import { store } from "..";
import { loadingAction } from "../reducer/loading";
import { GuidIdentityEntity, Entity } from "../model";

class SigoApiWithLoading<T extends Entity> extends SigoApi<T> {

    private loading: boolean;

    constructor(entity: string, loading = true) {
        super(entity);
        this.getList = this.getList.bind(this);
        this.getById = this.getById.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        this.count = this.count.bind(this);
        this.promiseWithLoading = this.promiseWithLoading.bind(this);
        this.loading = loading;
    }

    async promiseWithLoading<P>(promise: Promise<P>): Promise<P> {
        var result = null;
        try {
            this.loading && store.dispatch(loadingAction(true));
            result = await promise;
        } finally {
            this.loading && store.dispatch(loadingAction(false));
        }
        return result;
    }

    getList(options: PaginationFindManyOptions<T>): Promise<PaginationResult<T>>;
    getList(options: FindManyOptions<T>): Promise<T[]>;
    getList(): Promise<T[]>;
    getList(options: PaginationFindManyOptions<T> | FindManyOptions<T> = {}): Promise<T[] | PaginationResult<T>> {
        return this.promiseWithLoading(super.getList(options));
    }

    getById(id: number | string | ConditionsOptions<T>, options: GetOptions<T> = {}): Promise<T> {
        return this.promiseWithLoading(super.getById(id, options));
    }

    insert(item: Partial<T>): Promise<T> {
        return this.promiseWithLoading(super.insert(item));
    }

    // insertWithId(item: Partial<T>): Promise<T> {
    //     return this.promiseWithLoading(super.insertWithId(item));
    // }

    update(id: number | string | ConditionsOptions<T>, item: Partial<T>) {
        return this.promiseWithLoading(super.update(id, item));
    }

    count(options: ConditionsOptions<T>): Promise<number> {
        return this.promiseWithLoading(super.count(options));
    }

    delete(id: number | string | ConditionsOptions<T>) {
        return this.promiseWithLoading(super.delete(id));
    }

}

export {
    SigoApiWithLoading as SigoApi
}