import AuthService from './AuthService'
import Settings from './Settings';
import { store } from '../index';
import { logoutAction } from '../reducer/authentication';
import { Entity } from '../model';

const DEFAULT_PERPAGE = 50

export interface FetchOptions {
    method?: string,
    body?: BodyInit,
    headers?: { [key: string]: string }
}

export function getRequestHeader(access_token: string): Record<string, string> {
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
    }
}

export function getBaseUrl(model: string) {
    const { apiProtocol, apiHost, apiPort, apiPath } = Settings.get();
    return `${apiProtocol}://${apiHost}:${apiPort}${apiPath}/${model}/`;
}


type ConditionOperator = '=' | '!=' | '<>' | '>' | '>=' | '<' | '<=' | '><' | 'in' | 'like';

type ArrayCondition<T> = [keyof T, ConditionOperator, ...any[]] | [string, ConditionOperator, ...any[]];

export type WhereCondition<T> = { [P in keyof T]?: any } | ArrayCondition<T> | ArrayCondition<T>[] | undefined;

export interface ConditionsOptions<T> {
    where?: WhereCondition<T>
}

type Prop<T> = keyof T;

export interface GetOptions<T> {
    select?: Prop<T>[];
    relations?: string[];
}

export interface FindOneOptions<T> extends ConditionsOptions<T>, GetOptions<T> { }

export type OrderBy = 'ASC' | 'DESC';

export interface FindManyOptions<T> extends FindOneOptions<T> {
    order?: { [P in keyof T]?: OrderBy },
    offset?: number;
    limit?: number;
    //pagination?: boolean;
}

export interface PaginationFindManyOptions<T> extends FindManyOptions<T> {
    pagination: true;
}

export interface PaginationResult<T> {
    data: T[];
    count: number;
}

type METHOD = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'OPTIONS';

export async function promiseRequest(url: string, method?: METHOD, body?: Object) {
    try {
        var access_token = AuthService.instance.getAccessToken();
        if (!access_token) {
            throw new Error('Não autorizado');
        }
        var options: FetchOptions = {
            method: method || 'GET',
            headers: getRequestHeader(access_token)
        }
        if (body) {
            options.body = typeof body === 'string' ? body : JSON.stringify(body)
        }

        var response = await fetch(url, options);
        const text = await response.text();
        if (response.ok) {
            try {
                return (typeof text === 'string' ? JSON.parse(text) : null);
            } catch (e) {
                return text;
            }
        } else {
            if (response.status === 401 || response.status === 403) {
                await AuthService.instance.logout();
                store.dispatch(logoutAction());
            }
            throw new Error(text);
        }
    } catch (e) {
        console.log('Request Error', e);
        return null;
    };
}

async function promiseGetRequest(url: string) {
    return promiseRequest(url, 'GET');
}

export class SigoApi<T extends Entity> {

    public useLimit: boolean;

    constructor(protected model: string) {
        this.getUrl = this.getUrl.bind(this);
        this.getBaseUrl = this.getBaseUrl.bind(this);
        this.getList = this.getList.bind(this);
        this.getById = this.getById.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        //this.insertWithId = this.insertWithId.bind(this);
        this.useLimit = false;
    }

    getBaseUrl() {
        var model = this.model;
        return getBaseUrl(model);
    }

    count(options: ConditionsOptions<T>): Promise<number> {
        var { where } = options;
        var baseUrl = this.getBaseUrl();
        const params = this.getWhereParams(where);
        var url = baseUrl + `count/?` + params.join('&');
        return promiseGetRequest(url);
    }

    getWhereParams(where?: WhereCondition<T>) {
        var params = [];
        if (Array.isArray(where)) {
            var [p, ...rest] = where;
            if (typeof p === 'string') { //unica condição
                params.push(`where=${p},${rest.join(',')}`);
            } else {
                where.forEach((w, i) => {
                    var whereParam = `where[${i}]`;
                    if (Array.isArray(w)) {
                        var [p, ...rest] = w;
                        if (typeof p === 'string') {
                            params.push(whereParam + `=${p},${rest.join(',')}`);
                        } else if (Array.isArray(p)) {
                            w.forEach((c, i) => {
                                var w2 = whereParam + `[${i}]`;
                                if (Array.isArray(c)) {
                                    var [p, ...rest] = c;
                                    if (typeof p === 'string') {
                                        params.push(w2 + `=${p},${rest.join(',')}`);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        } else if (typeof where === 'object' && where !== null) {
            for (var prop in where) {
                var value = where[prop];
                params.push(`where[]=${prop},=,${value}`);
            }
        }
        return params;
    }

    getUrl(url: string, options: FindManyOptions<T>): string;

    getUrl(url: string, options: PaginationFindManyOptions<T>): string {
        var { limit, offset, order, where, select, relations, pagination } = options;
        var params = [];
        if (this.useLimit || pagination) {
            var o = offset || 0;
            var l = limit || DEFAULT_PERPAGE;
            params.push(`offset=${o}`);
            params.push(`limit=${l}`);
        }
        const whereParams = this.getWhereParams(where);
        params.push(...whereParams);
        var key = '';
        if (select) {
            for (key in select) {
                var filter = select[key];
                params.push(`select[]=${filter}`);
            }
        }
        if (order) {
            for (var orderKey in order) {
                var ord = order[orderKey];
                params.push(`order[${orderKey}]=${ord}`);
            }
        }
        if (Array.isArray(relations)) {
            for (key in relations) {
                var relation = relations[key];
                params.push(`relations[]=${relation}`);
            }
        }
        if (pagination) {
            params.push(`pagination=true`);
        }
        return url + '?' + params.join('&');
    }


    getList(options: PaginationFindManyOptions<T>): Promise<PaginationResult<T>>;
    getList(options: FindManyOptions<T>): Promise<T[]>;
    getList(): Promise<T[]>;

    getList(options: PaginationFindManyOptions<T> | FindManyOptions<T> = {}): Promise<T[] | PaginationResult<T>> {
        var baseUrl = this.getBaseUrl();
        var url = baseUrl + `list/`;
        url = this.getUrl(url, options);
        return promiseGetRequest(url);
    }

    getById(id: number | string | ConditionsOptions<T>, options: GetOptions<T> = {}): Promise<T> {
        var baseUrl = this.getBaseUrl();
        if (typeof id === 'object') {
            var url = this.getUrl(baseUrl, id);
        } else {
            var url = this.getUrl(baseUrl + `${id}/`, options);
        }
        return promiseGetRequest(url);
    }

    update(idOrOptions: number | string | ConditionsOptions<T>, item: Partial<T>) {
        var baseUrl = this.getBaseUrl();
        var url = baseUrl;
        if (typeof idOrOptions === 'object') {
            const options = idOrOptions;
            const { where } = options;
            url = this.getUrl(baseUrl, { where });
        } else {
            const id = idOrOptions;
            url = baseUrl + `${id}/`;
            //delete item.id;
            //delete item.guid;
        }

        return promiseRequest(url, 'PUT', item)
    }

    insert(item: Partial<T>): Promise<T> {
        var baseUrl = this.getBaseUrl();
        //delete item.id;
        //delete item.guid;
        return promiseRequest(baseUrl, 'POST', item);
    }

    // insertWithId(item: Partial<T>): Promise<T> {
    //     return promiseRequest(this.getBaseUrl(), 'POST', item);
    // }

    delete(idOrOptions: number | string | ConditionsOptions<T>) {
        var baseUrl = this.getBaseUrl();
        var url = baseUrl;
        if (typeof idOrOptions === 'object') {
            const options = idOrOptions;
            const { where } = options;
            url = this.getUrl(baseUrl, { where });
        } else {
            const id = idOrOptions;
            url = baseUrl + `${id}/`;
        }
        return promiseRequest(url, 'DELETE');
    }
}

export async function promiseGetImage(url: string) {
    var accessToken = AuthService.instance.getAccessToken();
    var options = {
        headers: {
            'Authorization': `Bearer ${accessToken}`
        }
    };
    return fetch(url, options).then(response => {
        if (response && response.ok) {
            return response.blob();
        } else {
            return null;
        }
    }).then(blob => {
        if (blob) {
            return URL.createObjectURL(blob);
        } else {
            return null;
        }
    });
}

export async function promiseUploadFile(url: string, file: File) {
    var accessToken = AuthService.instance.getAccessToken();
    var formData = new FormData();
    formData.append('file', file);
    var options = {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            'Authorization': `Bearer ${accessToken}`
        },
        body: formData
    };
    return fetch(url, options).then(async response => {
        if (response) {
            const text = await response.text();
            if (response.ok) {
                return (text ? JSON.parse(text) : null);
            } else {
                throw new Error(text);
            }
        }
    });
}
