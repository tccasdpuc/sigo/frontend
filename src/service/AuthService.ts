import { getBaseUrl } from "./SigoApi";

function saveAuthToken(authResponse: AuthResponse) {
	return localStorage.setItem("authTokens", JSON.stringify(authResponse));
};

function clearAuthToken() {
	return localStorage.removeItem("authTokens");
};

function getAuthToken(): AuthResponse {
	var token = localStorage.getItem("authTokens");
	return typeof token === "string" ? JSON.parse(token) : null;
};

export interface AuthResponse {
	accessToken: string;
}

class LoginRequest {

	constructor(public username: string, public password: string) {
		this.username = username;
		this.password = password;
	}
}

let _instance: AuthService;

export default class AuthService {

	static get instance() {
		if (_instance == null) {
			_instance = new AuthService();
		}
		return _instance;
	}

	constructor() {
		this.login = this.login.bind(this);
		this.logout = this.logout.bind(this);
		this.getAccessToken = this.getAccessToken.bind(this);
	}


	getAccessToken() {
		var data = getAuthToken();
		return data !== null ? data.accessToken : null;
	}

	async login(username: string, password: string) {
		var request = new LoginRequest(username, password);
		var options = {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			},
			body: JSON.stringify(request)
		};
		var token: AuthResponse;
		var url = getBaseUrl('api/auth') + 'login/';
		const response = await fetch(url, options);
		if (response.ok) {
			token = await response.json();
			saveAuthToken(token);
			return true;
		}
		else {
			clearAuthToken();
			throw new Error("Login Inválido");
		}
	}

	async logout() {
		var tokens = getAuthToken();
		if (tokens) {
			var access_token = tokens.accessToken;
			var options = {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: `Bearer ${access_token}`
				},
			};
			var url = getBaseUrl('api/auth') + 'logout/';
			try {
				await fetch(url, options);
			} catch (e) {
				console.error(e);
			}
		}
		clearAuthToken();
	}

	async validToken(): Promise<boolean> {
		var tokens = getAuthToken();
		if (tokens) {
			var access_token = tokens.accessToken;
			var options = {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: `Bearer ${access_token}`
				},
			};
			var url = getBaseUrl('api/auth') + 'validToken/';
			try {
				var response = await fetch(url, options);
				return response.ok;
			} catch (e) {
				console.error(e);
			}
		}
		return false;
	}

}
