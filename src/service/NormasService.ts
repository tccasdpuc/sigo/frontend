import Norma from '../model/Norma';
import NormaExterna from '../model/NormaExterna';
import { promiseRequest } from './SigoApi';
import { SigoApi } from './SigoApiWithLoading';

export default class NormasService extends SigoApi<Norma>{
    constructor() {
        super('normas-api');
    }

    async getNormasExternas(busca: string, codigoCategoria: string): Promise<NormaExterna[]> {
        const pageSize = 10;
        const url = this.getBaseUrl() + `pergamum/normas?busca=${busca}&codigoCategoria=${codigoCategoria}&pageSize=${pageSize}`;
        return await this.promiseWithLoading(promiseRequest(url));
    }
}