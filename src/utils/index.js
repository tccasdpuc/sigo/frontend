import * as fns from 'date-fns';
import { TreeEntity } from './index';
import Settings from '../service/Settings';

/**
* @param {Array<T>} array
* @param {keyof T| (keyof T)[]} propName
*/
export function sortObjectArrayByProp(array, propName) {
    return array.sort((a, b) => {
        if (Array.isArray(propName)) {
            for (var i = 0; i < propName.length; i++) {
                var p = propName[i];
                if (a[p] > b[p]) {
                    return 1;
                }
                if (a[p] < b[p]) {
                    return -1;
                }
            }
        } else {
            if (a[propName] > b[propName]) {
                return 1;
            }
            if (a[propName] < b[propName]) {
                return -1;
            }
        }
        return 0;
    });
}

/**
 * 
 * @param {Map<any,T>} map 
 * @returns {T[]}
 */
export function MapToArray(map) {
    return Array.from(map).map(([key, value]) => value);
}

/**
 * 
 * @param {Array<T>} array 
 * @param {string} prop 
 * @returns {Map<string,T[]>}
 */
export function groupByProp(array, prop) {
    var map = new Map();
    array.forEach(item => {
        var value = item[prop];
        var val = typeof value === 'object' ? JSON.stringify(value) : String(value);
        if (map.has(val)) {
            map.get(val).push(item);
        } else {
            map.set(val, [item]);
        }
    });
    return map;
}

/**
 * 
 * @param {Array<T>} array 
 * @returns {Array<T>}
 */
export function removeDuplicated(array) {
    return array.filter((v, i, a) => a.lastIndexOf(v) === i);
}

/**
 * 
 * @param {TreeEntity<T>} tree 
 */
export function treeToList(tree) {
    const list = [];
    const children = tree.children;
    if (Array.isArray(children) && children.length > 0) {
        children.forEach(child => {
            list.push(...(treeToList(child)));
        });
    }
    Object.assign(tree, { children: undefined });
    return [tree, ...list];
}

/**
 * 
 * @param {TreeEntity[]} list 
 * @param {*} rootGuid 
 */
export function listToTree(list, rootGuid) {
    if (Array.isArray(list)) {
        const root = list.find(node => node.guid === rootGuid);
        if (!root) return null;
        root.children = list
            .filter(child => child.parentGuid === root.guid)
            .map(child => listToTree(list, child.guid));
        return root;
    }
    return null;
}

export function accentedPhrase(phrase) {
    const phraseArray = String(phrase).toLowerCase().split(' ');
    const phraseSanitized = phraseArray.map(sanitizePhrase);
    console.log(phraseSanitized);
}

function sanitizePhrase(phrase) {
    const charArray = String(phrase).toLowerCase().split('');
    const charsAccented = charArray.map(char => chars(char));
    const phraseCount = charArray.length;
    const phrases = [phrase];

    for (var phraseIndex = 0; phraseIndex < phraseCount; phraseIndex++) {
        var char = charsAccented[phraseIndex];
        if (char) {
            var charCount = char.length;
            for (var charIndex = 0; charIndex < charCount; charIndex++) {
                if (phraseIndex === 0) {
                    phrases.push(char[charIndex] + String(phrase).substr(1, phraseCount));
                } else {
                    phrases.push(String(phrase).substr(0, phraseIndex) + char[charIndex] + String(phrase).substr(phraseIndex + 1, phraseCount));
                }
            }
        }
    }
    return phrases;
}

function chars(char) {
    const charsAccented = {
        'a': ['á', 'â', 'ã'],
        'á': ['a', 'â', 'ã'],
        'â': ['á', 'a', 'ã'],
        'ã': ['á', 'â', 'a'],

        'e': ['é', 'ê'],
        'é': ['e', 'ê'],
        'ê': ['é', 'e'],

        'i': ['í', 'î'],
        'í': ['i', 'î'],
        'î': ['í', 'i'],

        'o': ['ó', 'ô', 'õ'],
        'ó': ['o', 'ô', 'õ'],
        'ô': ['ó', 'o', 'õ'],
        'õ': ['ó', 'ô', 'o'],

        'u': ['ú', 'û'],
        'ú': ['u', 'û'],
        'û': ['ú', 'u'],

        'c': ['ç'],
        'ç': ['c'],

        'n': ['ñ'],
        'ñ': ['n'],
    }
    return charsAccented[char];
}

export async function delay(milliseconds) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, milliseconds);
    });
}

export function objectEquals(a, b) {
    const _objectEquals = (a, b, deepCount) => {
        if (!a && !b) return true;
        if (typeof a !== 'object' || typeof b !== 'object') {
            return a === b;
        }
        if (deepCount > 50) return false;
        const props = Object.getOwnPropertyNames(a);
        for (var i = 0; i < props.length; i++) {
            var propName = props[i];
            var propA = a[propName], propB = b[propName];
            if (propA === propB) {
                continue;
            }
            if (typeof propA === 'object' && typeof propB === 'object') {
                if (_objectEquals(propA, propB, deepCount + 1)) {
                    continue;
                }
            }
            return false;
        }
        return true;
    }
    try {
        return _objectEquals(a, b, 1) && _objectEquals(b, a, 1);
    } catch (e) {
        return false;
    }
}

/**
 * 
 * @param {Object} source 
 * @returns {Object}
 */
export function objectClone(source) {
    if (Array.isArray(source)) {
        return source.map(v => objectClone(v));
    } else if (source !== null && typeof source === 'object') {
        var clone = {};
        for (var prop in source) {
            Object.assign(clone, { [prop]: objectClone(source[prop]) });
        }
        return clone;
    } else {
        return source;
    }
}

export const ipRegExp = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

/**
 * 
 * @param {()=>boolean} predicate 
 * @param {number} timeout 
 * @param {number} step 
 */
export async function asyncPredicate(predicate, timeout = 1000, step = 10) {
    if (typeof predicate !== 'function') return false;
    for (var i = 0; i < (timeout / step); i++) {
        if (predicate()) return true;
        await delay(step);
    }
    return false;
}

/**
 * 
 * @param {string} message 
 * @param {Function} onclick 
 * @returns {Promise<Boolean>}
 */
export async function windowNotity(message, onclick) {
    var permission = window.Notification.permission;
    if (permission !== 'granted') {
        permission = await window.Notification.requestPermission();
    }
    if (permission === 'granted') {
        const notification = new window.Notification('IndTexBr SIGO', { body: message });
        notification.onclick = onclick;
        return true;
    }
    return false;
}

export function getReactElementText(element) {
    const pieces = [];
    if (typeof element === 'string' || typeof element === 'number') {
        pieces.push(element);
    } else if (typeof element === 'object') {
        if (!element) return '';
        const { props } = element;
        if (typeof props === 'object') {
            const { children } = props;
            if (Array.isArray(children)) {
                pieces.push(...children.map(c => getReactElementText(c)));
            } else if (typeof children === 'string') {
                pieces.push(children);
            }
        }
    }
    return pieces.join('');
}

export function popupwindow(url, title, w, h) {
    var left = (window.screen.width / 2) - (w / 2);
    var top = (window.screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

export function downloadBlob(blob, type, filename) {
    var a = window.document.createElement("a");
    var url = window.URL.createObjectURL(blob, { type });
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
}

/**
 * 
 * @param {Date} dtIni 
 * @param {Date} dtEnd 
 * @returns {Date[]}
 */
export function getDateDays(dtIni, dtEnd) {
    var diferencaDias = parseInt(fns.differenceInDays(dtEnd, dtIni), 10);
    var dates = [];
    var currentDate = dtIni;
    for (var d = 0; d <= diferencaDias; d++) {
        dates.push(currentDate);
        currentDate = fns.addDays(currentDate, 1);
    }
    return dates;
}

/**
 * 
 * @param {string} strCPF 
 */
export function TestCPF(strCPF) {
    var Soma = 0;
    var Resto = 0;
    strCPF = strCPF.replace(/(\.|-)/g, '');
    if (strCPF == "00000000000") return false;
    for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
}

export const fnsFormatFullDate = 'dd/MM/yyyy HH:mm:ss';

export const safePasswordRegex = /^(((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})/; //Letras e numeros, minimo oito caracteres