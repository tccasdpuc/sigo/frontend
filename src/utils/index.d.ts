interface TreeEntity<T> {
    guid: string;
    parentGuid: string;
    parent?: T;
    children: T[];
}

export function sortObjectArrayByProp<T>(array: T[], prop: keyof T | (keyof T)[]): T[];

export function MapToArray<T>(map: Map<any, T>): T[];
export function groupByProp<T extends Object>(array: T[], prop: string): Map<string, T[]>;
export function removeDuplicated<T>(array: T[]): T[];

export function listToTree(list: T[], rootGuid): T
export function treeToList<T extends TreeEntity<T>>(tree: T): T[];

export function delay(milliseconds: number): Promise<void>;

export function getFloorPlanItemImage(imageName: string, status: boolean = true): string;
export function objectEquals(a: Object, b: Object): boolean;
export function objectClone<T>(source: T): typeof source;

export const ipRegExp: RegExp;

export async function asyncPredicate(predicate: () => boolean, timeout = 1000, step = 10): Promise<boolean>;

export function windowNotity(message: string, onclick?: Function): Promise<Boolean>;

export function getReactElementText(element: React.ReactNode): string;

export function popupwindow(url: string, title: string, w: number, h: number): Window;

export function downloadBlob(blob: Blob, type: string, filename: string): void;

export function getDateDays(dtIni: Date, dtEnd: Date): Date[];

export function TestCPF(strCPF: string): boolean;

export const fnsFormatFullDate: string;

export const safePasswordRegex: RegExp;