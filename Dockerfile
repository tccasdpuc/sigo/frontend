FROM node:8-alpine as build-stage

WORKDIR /app
COPY package*.json /app/

RUN ["npm","install"]
RUN ["npm","install","react-scripts@3.4.3","-g"]

COPY ./ /app/
RUN ["rm","/app/public/Settings.json"]
RUN ["npm","run","build"]

FROM nginx:1.15.2-alpine
COPY --from=build-stage /app/build /var/www
COPY --from=build-stage /app/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]
